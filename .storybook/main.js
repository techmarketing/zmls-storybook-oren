module.exports = {
  "stories": [
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@storybook/addon-links",
    // "@storybook/preset-create-react-app",
    "@storybook/addon-essentials",
    "@storybook/addon-postcss",
    "@storybook/addon-backgrounds",
    "storybook-rtl-addon",
  ],
}

import '../src/index.css';
import addons from '@storybook/addons';
import { Direction_MODE_EVENT_NAME } from 'storybook-rtl-addon';

export const parameters = {
  backgrounds: {
    // default: 'twitter',
    values: [
      {
        name: 'twitter',
        value: '#00aced',
      },
      {
        name: 'facebook',
        value: '#3b5998',
      },
    ],
  },
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}

document.body.style.direction = 'rtl';

addons.getChannel().on(Direction_MODE_EVENT_NAME, direction =>  {
  document.body.style.direction = direction;
});

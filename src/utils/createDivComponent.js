export const createDivComponent = classNames => ({className, ...props}) =>  <div 
    className={[classNames, className].filter(Boolean).join(' ')}
    {...props} />
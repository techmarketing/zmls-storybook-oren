import React from 'react'
import PropTypes from 'prop-types'

export const AvailableDateCard = ({ className ,date = Date.now(), rounded, active = false ,...props}) => {


    const timeToHebrewWeekdayFormatter = (date) => {
        const dayOfTheWeekInNumber = new Date(date).getDay()+1
        let weekdayInLetter = ""
        switch (dayOfTheWeekInNumber) {
            case 1:
                weekdayInLetter = "א"
                break;
            case 2:
                weekdayInLetter = "ב"
                break;
            case 3:
                weekdayInLetter = "ג"
                break;
            case 4:
                weekdayInLetter = "ד"
                break;
            case 5:
                weekdayInLetter = "ה"
                break;
            case 6:
                weekdayInLetter = "ו"
                break;
            case 7:
                weekdayInLetter = "ש"
                break;
        
            default:
                break;
        }
        return "יום " + weekdayInLetter + "׳"
    }

    return (
        <div
        {...props}
        className={[
            'border-2 min-w-12 border-gray-200  shadow-sm flex flex-col justify-center items-center select-none px-2 py-4 h-20 w-20',
            rounded ? 'rounded-full' : 'rounded',
            active && 'border-green-500 bg-primary bg-opacity-10 text-primary font-open-sans-hb-regular',
            !active && 'text-gunmetal font-open-sans-hb-regular',
            className
        ].filter(Boolean).join(" ")}
        >
            <div
            className={["text-xl", active ?  "font-open-sans-hb-regular" : "font-open-sans-hb-regular"].filter(Boolean).join(" ")}
            >{("0" + (new Date(date).getDate())).slice(-2)}/{("0" + (new Date(date).getMonth()+1)).slice(-2)}</div>
            <div
            className="text-xl "
            >{timeToHebrewWeekdayFormatter(date)}</div>
        </div>
    )
}

AvailableDateCard.propTypes = {
    active: PropTypes.bool
}

AvailableDateCard.defaultProps = {
    
}


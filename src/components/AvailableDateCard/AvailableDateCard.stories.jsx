import React from 'react';

import { AvailableDateCard } from '.';

export default {
  title: 'atoms/AvailableDateCard',
  component: AvailableDateCard,
  parameters: {
    backgrounds: {
      values: [
        { name: 'red', value: '#f00' },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    className: {
        name: 'CSS Classes',
        control: {type: 'text'}
    }
  },
  args: {
    active: false
  }
};

const Template = (args) => <AvailableDateCard {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // date: Da
};


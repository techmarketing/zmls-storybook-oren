import React from "react";
import PropTypes from "prop-types";
import { CardContainer } from "../CardContainer";
import { Title } from "../Title";
import { Divider } from "../Divider";
import { Link } from "../Link";

export const LastAppointmentCard = ({ appointment, className, ...props }) => {
  return (
    <div className={[className, "w-3/4"].filter(Boolean).join(" ")}>
      <CardContainer>
        <div className="flex flex-col gap-2 p-4">
          <Title
            font="bold"
            className="text-gunmetal tracking-wider"
          >{appointment.tipulName}</Title>
          {Object.keys(appointment)?.map((topic) =>
            topic !== "tipulName" ? (
              <div className="flex">
                <Title
                  className="w-full text-md tracking-wider text-gunmetal text-sm"
                  font="regular"
                >{appointment[topic]}</Title>
              </div>
            ) : (
              ""
            )
          )}
        </div>

        <Divider className="" />

        <div className="p-2 px-4">
          <Link className="font-open-sans-hb-bold pr-1">זימון נוסף</Link>
        </div>
      </CardContainer>
    </div>
  );
};

LastAppointmentCard.propTypes = {
  appointment: PropTypes.object.isRequired,
};

LastAppointmentCard.defaultProps = {
  
};

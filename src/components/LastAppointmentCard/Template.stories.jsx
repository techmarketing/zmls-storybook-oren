import React from 'react';


import { LastAppointmentCard } from '.';

export default {
  title: 'molecules/LastAppointmentCard',
  component: LastAppointmentCard,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
  args: {
    appointment: {
      tipulName: "דיקור סיני אנטי אייג׳ינג",
      dr: "ירושלמי בן הרוש שלומית",
      clinic: "מרפאת קיראון",
    },
  }
};

const Template = (args) => <LastAppointmentCard {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


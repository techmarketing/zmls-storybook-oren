import React from "react";
import PropTypes from "prop-types";
import { Title } from "../Title";
import { CardContainer } from "../CardContainer";
import { TiplulimMeusharim } from "../TiplulimMeusharim";

export const TipulimMeusharimMolecule = ({
  tiplulimMeusharimArr,
  className,
  ...props
}) => {
  return (
    <div {...props} className={[className].filter(Boolean).join(" ")}>
      <Title className="text-subtitle-blue" font="regular">
        טיפולים מאושרים לזימון תור:
      </Title>
      <CardContainer className={"mt-3 p-6"}>
        <div>
          {tiplulimMeusharimArr?.map((t) => (
            <TiplulimMeusharim >{t}</TiplulimMeusharim>
          ))}
        </div>
        <div className="flex mt-3 flex-row items-center relative">
          <span className="absolute top-0 right-0 flex justify-center items-center">ast</span>
          <span className="w-2/12"></span>
          <div className="w-10/12 flex flex-col">
            <Title
              
              font="bold"
              className="text-subtitle-golden tracking-wider text-lg transform translate-x-1 text-right"
            >מחפשים טיפול אחר?</Title>
            <Title
              font="regular"
              className="text-gunmetal tracking-wider text-md pt-2 text-right transform translate-x-3"
            >ניתן ליצור קשר עם המוקד או המרפאה המטפלת לקבלת הרשאות לטיפולים נוספים.</Title>
          </div>
        </div>
      </CardContainer>
    </div>
  );
};

TipulimMeusharimMolecule.propTypes = {
  tiplulimMeusharimArr: PropTypes.array,
};

TipulimMeusharimMolecule.defaultProps = {
 
};

import React from 'react';


import { TipulimMeusharimMolecule } from '.';

export default {
  title: 'molecules/TipulimMeusharimMolecule',
  component: TipulimMeusharimMolecule,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
  args:{
    tiplulimMeusharimArr: [
      "טווינא",
      "נתורופתיה",
      "דיקור סיני אנטי אייג׳ינג",
      "תור לייעוץ",
    ],
  }
};

const Template = (args) => <TipulimMeusharimMolecule {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


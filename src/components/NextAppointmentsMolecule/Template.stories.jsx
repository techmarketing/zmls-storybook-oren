import React from 'react';


import { NextAppointmentsMolecule } from '.';

export default {
  title: 'molecules/NextAppointmentsMolecule',
  component: NextAppointmentsMolecule,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
  args: {
    appointmentsArr: [
      {
        name: "טווינא",
        clinic: "מרפאת אשקלון סנטר",
        where: "גבעת השקמה 2, קריית אונו",
        date: "05.02.20 | יום ד׳",
        hour: "09:40",
        dr: "מטפל: ירושלמי בן הרוש שלומי",
      },
      {
        name: "נתורופתיה",
        clinic: "מרפאת אשקלון סנטר",
        where: "גבעת השקמה 2, קריית אונו",
        date: "05.02.20 | יום ד׳",
        hour: "09:40",
        dr: "מטפל: ירושלמי בן הרוש שלומי",
      },
      {
        name: "דיקור סיני אנטי אייג׳ינג",
        clinic: "מרפאת אשקלון סנטר",
        where: "גבעת השקמה 2, קריית אונו",
        date: "05.02.20 | יום ד׳",
        hour: "09:40",
        dr: "מטפל: ירושלמי בן הרוש שלומי",
      },
    ],
  }
};

const Template = (args) => <NextAppointmentsMolecule {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


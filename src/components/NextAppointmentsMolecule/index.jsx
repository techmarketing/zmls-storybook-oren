import React from "react";
import PropTypes from "prop-types";
import { Title } from "../Title";
import { NextAppointmentCard } from "../NextAppointmentCard";
import { CardContainer } from "../CardContainer";
import { Button } from "../Button";

export const NextAppointmentsMolecule = ({
  appointmentsArr,
  className,
  ...props
}) => {
  return (
    <div {...props} className={[className].filter(Boolean).join(" ")}>
      <Title
        font="regular"
        className="text-subtitle-blue tracking-wider "
      >{`התורים הבאים שלי (${appointmentsArr?.length}):`}</Title>
      <div className={["overflow-x-scroll pb-3"].filter(Boolean).join(" ")}>
        {appointmentsArr.length > 0 ? (
          <div className="flex gap-3 mt-4 border-2">
            {appointmentsArr?.map((a) => (
              <NextAppointmentCard className="box-border h-full border-2" appointment={a} />
            ))}
          </div>
        ) : (
          <CardContainer className="p-5 max-w-full">
              <Title font="regular" className="w-full text-center text-gunmetal tracking-wide">
                ברוך הבא לזימון התורים של רפואה משלימה, יש לזמן תור לייעוץ
                מקצועי{" "}
              </Title>
              <Button classNames="w-full">+ זמני תור לייעוץ</Button>
          </CardContainer>
        )}
      </div>
    </div>
  );
};

NextAppointmentsMolecule.propTypes = {
  appointmentsArr: PropTypes.array,
};

NextAppointmentsMolecule.defaultProps = {};

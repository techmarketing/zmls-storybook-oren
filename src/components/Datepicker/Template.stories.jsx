import React from 'react';


import { Datepicker } from '.';

export default {
  title: 'molecules/Datepicker',
  component: Datepicker,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
  args: {
    className: "font-open-sans-hb-bold bg-white",
    unavailableDaysArray: [(new Date(2021, 8, 15)), (new Date(2021, 9, 10)), new Date(2021, 9, 12), new Date(2021, 9, 13)],
    weekdaysToRemove: [6],
    dateRange: [],
    monthsToShow: 1,
  }
};

const Template = (args) => <Datepicker {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


import React, { useEffect, useState, useMemo } from 'react'
import PropTypes from 'prop-types'
import { ArrowButton } from "../ArrowButton";
import {useDatePickerArragner} from '../../functions/useDatePickerArragner'
import CloseIcon from '@material-ui/icons/Close';

const statuses = {
    CURRENT: 1,
    BLURRED: 2,
    ACTIVE: 4,
    DIFFMONTH: 8,
    HIDDEN: 16,
    CURRENTINACTIVE: 32,
};

statuses.CURRENT | statuses.ACTIVE;

const Day = ({day, status = 0, className, ...props}) => {
    const cssClass = useMemo(() => {
        return [
            (status & statuses.BLURRED) && 'opacity-50 pointer-events-none',
            (status & statuses.ACTIVE) && 'text-primary transition-all duration-200 hover:bg-primary hover:text-white rounded-full',
            (status & statuses.CURRENT) && 'bg-primary text-white rounded-full',
            (status & statuses.CURRENTINACTIVE) && 'bg-gray-400 text-white rounded-full pointer-events-none',
            (status & statuses.DIFFMONTH) && 'text-pencil-gray-light pointer-events-none',
            (status & statuses.HIDDEN) && 'opacity-0 pointer-events-none',
        ].filter(Boolean).join(' ');
    }, [status]);
    return <div {...props} className={[className,cssClass, "flex justify-center items-center w-8 h-8 cursor-pointer"].filter(Boolean).join(" ")}>{day.getDate()}</div>
}

const YearPopUp = ({ className, year, setDate, setPopup }) => {
    const months = ["ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני", "יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר"]
    const [currYear, setCurrYear] = useState(year)
    return <div 
    className={[className].filter(Boolean).join(" ")}>
        <div className="h-1/6 flex items-center justify-center px-4 bg-pencil-gray-light rounded-t">
            <CloseIcon onClick={()=>setPopup(false)} className="absolute right-4 top-4" />
            <div className="w-1/2 flex justify-between items-center">
                <ArrowButton onClick={()=>setCurrYear(currYear + 1)} />
                <div className="text-subtitle-blue text-3xl">{currYear}</div>
                <ArrowButton ltn={false} onClick={()=>setCurrYear(currYear - 1)} />                
            </div>
        </div>
        <div className="grid grid-cols-2 h-5/6">
            {months.map((m, index)=><div
            onClick={()=>{setDate(new Date(currYear, index));setPopup(false)}}
            className={["flex justify-center items-center bg-primary text-pencil-gray-light hover:text-primary hover:bg-white transition-all duration-100", index === new Date().getMonth() && "bg-blue"].filter(Boolean).join(" ")}>{m}</div>)}
        </div>
    </div>
}

// prop guidelines - dateRange[start, end](//date objs), unavailableDaysArray:dateObj[], weekdaysToRemove:number[]//representing indexes, so 0 is sunday
export const Datepicker = ({initialDate = new Date() ,title ,className, dateRange = [], weekdaysToRemove = [], unavailableDaysArray = [], monthsToShow = 1 ,...props}) => {

    const handleMonthChange = (dir) => {
        if(dir==="plus"){
            setDate(new Date(date.setMonth(date.getMonth()+1)))
            return
        }
        setDate(new Date(date.setMonth(date.getMonth()-1)))
    }

    const determineDayStatus = (day, month) => {

        // Date is not in the dateRange
        if( dateRange && (dateRange[0]?.getTime()>day.getTime() || dateRange[1]?.getTime()<day.getTime())){
            return 16
        }

        // check if the day is today and that there is none chosen
        if(!chosenDate && day.getDate() === new Date().getDate() && day.getMonth() === new Date().getMonth() && day.getFullYear() === new Date().getFullYear()) {
            if(unavailableDaysArray.some(d=>d.getTime()===day.getTime())){
                return 32
            }
            return 1
        }

        // check if the day is the chosen one
        if(chosenDate && chosenDate===day){
            return 1
        }
    
        // check if the day is unavailable or before today
        if(unavailableDaysArray.some(d=>d.getTime()===day.getTime()) || day.getTime() < new Date()){
            return 2
        }

        // Date is not of this month
        if (day.toLocaleString('default', { month: 'long' }) !== month.split(2)[0]){
            return 8
        }
    
        // else it's active
        return 4
    }

    const weekdays = useMemo(() => {
        return [
            'א',
            'ב',
            'ג',
            'ד',
            'ה',
            'ו',
            'ש',
        ].filter((weekDay, i)=>!weekdaysToRemove.some(index=> index===i))
    }, weekdaysToRemove)

    const [chosenDate, setChosenDate] = useState(null)
    const [popup, setPopup] = useState(false)

    const [date, setDate] = useState(initialDate)
    
    // this is an object containing arrays representing months and thedays of it ordered by weeks
    const calendarData = useMemo(() => {
        return useDatePickerArragner(date || dateRange[0], dateRange[1]?.getMonth()-dateRange[0]?.getMonth()+1 || monthsToShow, weekdaysToRemove)
    }, [date])

    return (
        <div className="">
            {Object.keys(calendarData).map(month=>(
                <div
                key={month}
                className={[
                    className,
                    "rounded shadow select-none box-border p-2 relative"
                ].filter(Boolean).join(" ")}
                >
                    <div className="flex justify-end items-center"><button className="close-btn w-full text-right p-3 w-min"><CloseIcon /></button></div>
                    {/* {monthsToShow===1 && <div className="title w-full text-center text-subtitle-blue text-2xl font-open-sans-hb-bold">{title}</div>} */}
                    {<div className="flex justify-center items-center"><div onClick={()=>{setPopup(true);console.log('clock')}} className="mt-1 mb-3 cursor-pointer w-max title w-full text-center text-subtitle-blue text-2xl font-open-sans-hb-bold">{calendarData[month][1][5].toLocaleString('he-IL', { month: 'long' })}</div></div>}
                    {monthsToShow===1 && 
                    <div className="navbar w-full flex justify-between px-5 my-5">
                        <ArrowButton onClick={()=>handleMonthChange()} ltn={false} />
                        <ArrowButton onClick={()=>handleMonthChange("plus")} />
                        {/* <CalendarTodayIcon onClick={()=>{setDate(new Date());setChosenDate(null)}}>return_to_today</CalendarTodayIcon> */}
                    </div>
                    }
                    {/* {<div className="flex justify-center items-center"><div onClick={()=>{setPopup(true);console.log('clock')}} className="mt-1 mb-6 cursor-pointer w-max">{calendarData[month][1][5].toLocaleString('he-IL', { month: 'long' }) + " " + currDate.getFullYear()}</div></div>} */}
                    <div className={`letter-bar gap-2 md:gap-4 lg:gap-8 grid grid-cols-${weekdays.length}`}>
                        
                        {weekdays.map(day=><div key={day} className="w-full flex justify-center font-open-sans-hb-regular items-center text-subtitle-gray text-xl">{day}</div>)}
                    </div>
                    <div className="date-grid gap-4 flex flex-col mt-3 ">
                        {calendarData[month].map(week=>(
                            <div key={month + week} className={`grid gap-2 md:gap-4 lg:gap-8 grid-cols-${weekdays.length}`}>
                                {week.map(day=> <div className="flex justify-center items-center"><Day onClick={()=>{console.log("The date " + day + " is available!");setChosenDate(day)}} className="font-open-sans-hb-light" key={day} day={day} status={determineDayStatus(day, month)} /></div>)}
                            </div>
                        ))}
                    </div>
                    {/* <div>{popup.toString()}</div> */}
                    <YearPopUp 
                    setDate={setDate} setPopup={setPopup}
                    year={currDate.getFullYear()}
                    className={["absolute left-0 w-full h-full top-0 bg-white rounded transition-all duration-500 transform", popup ? "opacity-8 translate-y-0" : "opacity-0 bot-0 translate-y-full"].filter(Boolean).join(" ")} />
                </div>
            ))}
        </div>
    )
}

Datepicker.propTypes = {
    unavailableDaysArray: PropTypes.array,
    weekdaysToRemove: PropTypes.array,
    className: PropTypes.string,
    title: PropTypes.string,
    monthsToShow: PropTypes.number,
}

Datepicker.defaultProps = {
    
}


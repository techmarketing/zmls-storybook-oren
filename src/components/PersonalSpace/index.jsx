import React, { useState } from "react";
import { CardContainer } from "../CardContainer";
import { AvatarGallery } from "../AvatarGallery";
import { Title } from "../Title";

export const PersonalSpace = ({  fatherFoo ,profilesArr ,className, ...props }) => {

    const [currProfileIndex, setCurrProfileIndex] = useState(0)

  return (
    <CardContainer
      {...props}
      className={[className, "shadow-lg p-4 flex items-center justify-between"]
        .filter(Boolean)
        .join(" ")}
    >
      <div className="w-1/2">
        <AvatarGallery
          fatherFoo2={fatherFoo}
          profilesArr={profilesArr}
          className="w-full"
          height={20}
          fatherFoo1={setCurrProfileIndex}
        />
      </div>
      <div className="flex justify-center items-center w-1/2">
        <Title font="regular" className="tracking-wide text-lg text-gunmetal">
          {profilesArr && (profilesArr[currProfileIndex].fname + " " + profilesArr[currProfileIndex].lname)}
        </Title>
      </div>
    </CardContainer>
  );
};

PersonalSpace.propTypes = {};

PersonalSpace.defaultProps = {};

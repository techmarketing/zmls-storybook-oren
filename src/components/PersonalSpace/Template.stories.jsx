import React from "react";

import { PersonalSpace } from ".";

export default {
  title: "molecules/PersonalSpace",
  component: PersonalSpace,
  parameters: {
    backgrounds: {
      values: [
        { name: "fun gray", value: "#f7f9fc", default: true },
        { name: "green", value: "#0f0" },
        { name: "blue", value: "#00f" },
      ],
    },
  },
  argTypes: {},
  args: {
    profilesArr: [
      { fname: "דולב", lname: "גרוזנברג", id: "dolev_id" },
      { fname: "יוני", lname: "יוניהו", id: "yoni_id" },
      { fname: "קרן", lname: "ששון", id: "israel_id" },
      { fname: "מאיה", lname: "ששון", id: "sami_id" },
      { fname: "עופר", lname: "ששון", id: "ofer_id" },
    ],
  },
};

const Template = (args) => (
  <div className="min-h-screen bg-lightgray p-5">
    <PersonalSpace {...args} />
  </div>
);

export const Primary = Template.bind({});
Primary.args = {
  // label: 'Primary',
};

import React from 'react'
import { Divider } from '../Divider'
import { Title } from '../Title'
import { Button } from '../Button'

export const TiplulimMeusharim = ({className, children,...props}) => {
    return (
        <div {...props}
        className={[
            className
        ].filter(Boolean).join(" ")}
        >
            <div className="flex justify-between items-center my-3">
                <div className="flex w-full flex-row">
                    <span className="w-2/12">ast</span>
                    <Title className="text-gunmetal tracking-wider" font="regular" >{children}</Title>
                </div>
                <Button  className="" style="outline" >זימון</Button>
            </div>
            <Divider />
        </div>
    )
}

TiplulimMeusharim.propTypes = {
    
}

TiplulimMeusharim.defaultProps = {

}
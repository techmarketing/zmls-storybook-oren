import React from 'react';


import { TiplulimMeusharim } from '.';

export default {
  title: 'molecules/TiplulimMeusharim',
  component: TiplulimMeusharim,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
};

const Template = (args) => <TiplulimMeusharim {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


import React from 'react';


import { TreatmentsDoctors_old_00 } from '.';

export default {
  title: 'old/templates/TreatmentsDoctors_old_00',
  component: TreatmentsDoctors_old_00,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
  args: {
    availableAppointments: {
      ["ד״ר בן-דוד שלמה"]: [
        { time: "09:35", date: new Date() },
        { time: "09:50", date: new Date() },
        { time: "10:30", date: new Date() },
        { time: "11:15", date: new Date() },
        { time: "09:35", date: new Date() },
        { time: "09:50", date: new Date() },
        { time: "10:30", date: new Date() },
        { time: "11:15", date: new Date() },
        { time: "09:35", date: new Date() },
        { time: "09:50", date: new Date() },
        { time: "10:30", date: new Date() },
        { time: "11:15", date: new Date() },
      ],
      ["ד״ר אלטרמן מעיין"]: [
        { time: "09:50", date: new Date() },
        { time: "09:50", date: new Date() },
        { time: "09:50", date: new Date() },
        { time: "09:50", date: new Date() },
        { time: "09:35", date: new Date(2021, 11, 4) },
        { time: "10:30", date: new Date(2021, 11, 4) },
        { time: "12:15", date: new Date(2021, 11, 4) },
        { time: "09:50", date: new Date(2021, 11, 4) },
      ],
      ["ד״ר טל"]: [
        { time: "09:50", date: new Date() },
        { time: "09:50", date: new Date() },
        { time: "09:50", date: new Date() },
        { time: "09:50", date: new Date() },
        { time: "09:35", date: new Date(2022, 11, 5) },
        { time: "10:30", date: new Date(2022, 9, 4) },
        { time: "12:15", date: new Date(2022, 9, 5) },
        { time: "09:50", date: new Date(2022, 9, 6) },
      ],
      ["ד״ר אבי שפירו"]: [
        { time: "09:50", date: new Date() },
        { time: "09:50", date: new Date() },
        { time: "09:50", date: new Date() },
        { time: "09:50", date: new Date() },
        { time: "09:35", date: new Date(2022, 9, 7) },
        { time: "10:30", date: new Date(2022, 9, 8) },
        { time: "12:15", date: new Date(2022, 9, 9) },
        { time: "09:50", date: new Date(2022, 9, 4) },
      ],
      ["ד״ר רועי"]: [
        { time: "09:50", date: new Date() },
        { time: "09:50", date: new Date() },
        { time: "09:50", date: new Date() },
        { time: "09:50", date: new Date() },
        { time: "09:35", date: new Date(2022, 9, 1) },
        { time: "10:30", date: new Date(2022, 9, 2) },
        { time: "12:15", date: new Date(2022, 9, 5) },
        { time: "09:50", date: new Date(2022, 9, 22) },
      ],
    },
  }
};

const Template = (args) => <TreatmentsDoctors_old_00 {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


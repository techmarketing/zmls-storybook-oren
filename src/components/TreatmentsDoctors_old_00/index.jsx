import React, { useMemo, useState } from "react";
import PropTypes from "prop-types";
import { Header } from "../Header";
import { Title } from "../Title";
import { Link } from "../Link";
import { AvailableDatesGallery } from "../AvailableDatesGallery";
import { DrAvailableAppointments } from "../DrAvailableAppointments";

export const TreatmentsDoctors_old_00 = ({
  availableAppointments,
  className,
  ...props
}) => {
  // proccess the object to an array of dates
  const datesArr = useMemo(() => {
    const datesArr = [new Date()];
    for (const dr in availableAppointments) {
      for (const appointment of availableAppointments[dr]) {
        if (
          !datesArr.some(
            (d) =>
              d.getFullYear() === appointment.date.getFullYear() &&
              d.getMonth() === appointment.date.getMonth() &&
              d.getDate() === appointment.date.getDate()
          )
        ) {
          datesArr.push(appointment.date);
        }
      }
    }
    return datesArr;
  }, [availableAppointments, currDate]);

  const [currDate, setCurrDate] = useState(datesArr[0]);
  const [resetRadios, setResetRadios] = useState(false);

  

  // filter out the appointnments that are not of this date
  const availableAppointmentsSum = useMemo(() => {
    let sum = 0;
    for (const dr in availableAppointments) {
      sum += availableAppointments[dr].filter(
        (a) =>
          a.date.getFullYear() === currDate.getFullYear() &&
          a.date.getMonth() === currDate.getMonth() &&
          a.date.getDate() === currDate.getDate()
      ).length;
    }
    return sum;
  }, [availableAppointments, currDate]);

  return (
    <div {...props}
      className={[className, "bg-lightgray min-h-screen"]
        .filter(Boolean)
        .join(" ")}
    >
      <Header type="login" className="bg-white py-5" logoWidth="w-1/3" />

      <section className="p-4 shadow-inner">
        <div className="px-2">
          <div className="flex justify-center mb-4 mt-1 items-center">
            <Title
              font="bold"
              className="text-secondary text-3xl tracking-wider"
            >זימון תור לטיפול טווינא</Title>
          </div>
          <div className="flex justify-center items-center gap-2 my-2">
            <Title
              font="regular"
              className="text-gunmetal text-xl tracking-wide "
            >באיזור קרית אונו</Title>
            {/* <div>ast</div> */}
          </div>
          <div className="flex justify-center items-start my-5 gap-2 ">
            <div>ast</div>
            <Title
              font="regular"
              className="text-gunmetal secondary text-xl tracking-wide leading-5"
            >מרפאת קיראון - גבעת השקמה 2</Title>
          </div>
        </div>

        <div className='flex flex-col gap-4'>
          <Title
            className="text-secondary tracking-wider text-xl"
            font="regular"
          >תאריכים זמינים</Title>
          <AvailableDatesGallery
            resetRadios={resetRadios}
            setResetRadios={setResetRadios}
            setCurrDate={setCurrDate}
            availableDates={datesArr}
          />
        </div>
      </section>

      <section className="pb-3 px-3 ">
        <div className="flex justify-between items-center mb-7">
          <Title font="regular" className="text-secondary text-xl" >{`נמצאו ${availableAppointmentsSum} תורים:`}</Title>
          <Link className="font-open-sans-hb-regular text-xl">סינון תוצאות</Link>
        </div>

        <div className="flex flex-col gap-3 py-3">
            {Object.keys(availableAppointments).map((dr) => (
            <DrAvailableAppointments
                resetRadios={resetRadios}
                dr={dr}
                appointments={availableAppointments[dr].filter(
                (a) =>
                    a.date.getFullYear() === currDate.getFullYear() &&
                    a.date.getMonth() === currDate.getMonth() &&
                    a.date.getDate() === currDate.getDate()
                )}
            />
            ))}
        </div>
      </section>
      
    </div>
  );
};

TreatmentsDoctors_old_00.propTypes = {
  availableAppointments: PropTypes.object,
};

TreatmentsDoctors_old_00.defaultProps = {
  
};

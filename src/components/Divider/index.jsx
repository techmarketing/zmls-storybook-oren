import React from 'react'
import PropTypes from 'prop-types'

export const Divider = ({ className, titleType ,size ,...props}) => {
    return (
        <div
        {...props}
        className={[
            "h-0.5 bg-very-light-blue w-full",
            className
        ].filter(Boolean).join(" ")}
        >
            
        </div>
    )
}

Divider.propTypes = { 
   
}

Divider.defaultProps = { 
    
}

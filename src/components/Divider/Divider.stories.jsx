import React from 'react';

import { Divider } from '.';

export default {
  title: 'atoms/Divider',
  component: Divider,
  parameters: {
    backgrounds: {
      values: [
        { name: 'red', value: '#f00', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    children: {
        name: "Text",
        control: { type: 'text' }
    },
    className: {
        name: "CSS Classes",
        control: {type: "text"}
    }
  },
};

const Template = (args) => <Divider {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


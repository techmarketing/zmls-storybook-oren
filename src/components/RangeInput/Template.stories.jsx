import React from 'react';


import { RangeInput } from '.';

export default {
  title: 'atoms/RangeInput',
  component: RangeInput,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
  args: {
    labeled: true,
    className: "",
    min: "08:00",
    max: "20:00",
    labelGap: "mb-3",
  }
};

const Template = (args) => {
  console.log(args.onChange);
  return <div className="p-5"><RangeInput {...args} /></div>}

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};
// const Template2 = (args) => <div className={""} style={{width: '400px'}}>
//     <div>שעות קבלה</div>
//     <RangeInput dir="ltr" {...args} />
//   </div>;

// export const WithBlah = Template2.bind({});
// WithBlah.args = {
//     // label: 'Primary',
// };


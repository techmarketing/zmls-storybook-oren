import React, { useState, useMemo, useRef, useEffect } from "react";
import PropTypes from "prop-types";
import "./style.css";
import { Title } from "../Title"

const stringToNum = (string) => {
  if (typeof string !== "string" || string.indexOf(":") === -1) {
    return string;
  }
  const [hour = 0, min = 0] = string.split(":");
  return parseInt(hour) * 60 + parseInt(min);
};

const numToString = (num) => {
  const zerofy = (n) => ("0" + n).slice(-2);
  return `${zerofy(Math.floor(num / 60))}:${zerofy(num % 60)}`;
};

// https://jsfiddle.net/c8sp2k4h/131/

export const RangeInput = ({
  className,
  min = "08:00",
  max = "20:00",
  onChange = null,
  step = "4:00",
  labeled = true,
  labelGap = "mb-3",
  resetRange,
  ...props
}) => {
  const minVal = useMemo(() => stringToNum(min), [min]);
  const maxVal = useMemo(() => stringToNum(max), [max]);
  const stepVal = useMemo(() => stringToNum(step), [step]);
  const [direction, setDirection] = useState("rtl");
  const wrapper = useRef(null);
  const labels = useMemo(() => {
    const result = [];

    for (let i = minVal; i <= maxVal; i += stepVal) {
      result.push(i);
    }
    return result;
  }, [minVal, maxVal, stepVal]);

  const [selectedMin, setSelectedMin] = useState(minVal);
  const [selectedMax, setSelectedMax] = useState(maxVal);

  const from = useMemo(() => Math.min(selectedMin, selectedMax), [
    selectedMin,
    selectedMax,
  ]);
  const to = useMemo(() => Math.max(selectedMin, selectedMax), [
    selectedMin,
    selectedMax,
  ]);

  const minValRef = useRef(stringToNum(min));
  const maxValRef = useRef(stringToNum(max));
  const range = useRef(null);

  const selectedRangeStyle = useMemo(
    () => ({
      [direction === "rtl" ? "right" : "left"]:
        (100 * (from - minVal)) / (maxVal - minVal) + "%",
      width: (100 * (to - from)) / (maxVal - minVal) + "%",
    }),
    [from, to, direction]
  );

  useEffect(() => {
    setSelectedMin(stringToNum(min));
    setSelectedMax(stringToNum(max));
  }, [resetRange])

  useEffect(() => {
    setDirection(window?.getComputedStyle(wrapper.current)?.direction || "ltr");
  }, [wrapper]);

  useEffect(() => {
    onChange({
      from,
      to,
      fromName: numToString(from),
      toName: numToString(to),
    });
  }, [from, to]);

  return (
    // <>
      <div
        ref={wrapper}
        {...props}
        className={[ className, "box-border flex flex-col relative"].filter(Boolean).join(" ")}
      >
        {labeled && <div className="flex justify-between">
          {labels.map((i) => (
              <div className={["w-max", labelGap].filter(Boolean).join(" ")} key={i}>
                <Title font="light" className={["text-xl text-gunmetal tracking-wide "].filter(Boolean).join(" ")}>{numToString(i)}</Title>
              </div>
          ))}
        </div>}

        <div className="ranges">
          <div className="range-wrapper">
            <input
              type="range"
              name="input1"
              min={minVal}
              max={maxVal}
              value={selectedMin}
              step={stepVal}
              className={`thumb thumb--left`}
              style={{ zIndex: minVal > max - 100 && "5" }}
              onChange={(event) => {
                const value = Math.min(Number(event.target.value), maxVal);
                setSelectedMin(value);
                minValRef.current = value;
              }}
            />
          </div>
          <div className="range-wrapper">
            <input
              type="range"
              min={minVal}
              max={maxVal}
              step={stepVal}
              value={selectedMax}
              onChange={(event) => {
                const value = Math.max(Number(event.target.value), minVal);
                setSelectedMax(value);
                maxValRef.current = value;
              }}
              className={`thumb thumb--right`}
            />
          </div>
        </div>
        <div className={`slider relative`}>
          <div className="slider__track bg-primary" />
          <div
            style={{ right: ((from - minVal) / (maxVal - minVal)) * 100 + "%" }}
            className="circ bg-white shadow-lg border-2 w-5 h-5 rounded-full transform translate-x-1/2 pointer-events-none  z-10 -top-2 absolute"
          ></div>
          <div
            style={{
              right: 100 - ((maxVal - to) / (maxVal - minVal)) * 100 + "%",
            }}
            className="circ bg-white shadow-lg border-2 w-5 h-5 rounded-full transform translate-x-1/2 pointer-events-none  z-10 -top-2 absolute"
          ></div>

          <div
            ref={range}
            className="slider__range bg-primary"
            style={selectedRangeStyle}
          />
        </div>
      </div>
    // {/* </> */}
  );
};

RangeInput.propTypes = {
  min: PropTypes.number,
  max: PropTypes.number,
  onChange: PropTypes.func,
  labeled: PropTypes.bool,
};

RangeInput.defaultProps = {
    labeled: true,
};

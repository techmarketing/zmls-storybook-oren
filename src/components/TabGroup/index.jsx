import React from 'react'
import PropTypes from 'prop-types'
import {Tab} from '../Tabs'

export const TabGroup = ({  tabUnitClass ,defaultSelected ,childrenArr ,className, ...props}) => {
    return (
        <div {...props}
        className={[
            "flex",
            className,
        ].filter(Boolean).join(" ")}
        >
        {childrenArr?.map((c,i)=><Tab className={tabUnitClass} selected={i===defaultSelected} onClick={c.fatherFoo}>{c.label}</Tab>)}
        </div>
    )
}

TabGroup.propTypes = {
    childrenArr: PropTypes.array   
}

TabGroup.defaultProps = {


}
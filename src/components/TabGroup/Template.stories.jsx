import React from 'react';


import { TabGroup } from '.';

export default {
  title: 'old/molecules/TabGroup',
  component: TabGroup,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
  args: {
    childrenArr: [
      {label:"לשונית1", fatherFoo: ()=>console.log("Do something")},
      {label:"לשונית2", fatherFoo: ()=>console.log("Do something")},
      {label:"לשונית3", fatherFoo: ()=>console.log("Do something")},
      {label:"afer", fatherFoo: ()=>console.log("Do something")},
      {label:"ergwergw", fatherFoo: ()=>console.log("Do something")},
  ]   
  }
};

const Template = (args) => <TabGroup {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


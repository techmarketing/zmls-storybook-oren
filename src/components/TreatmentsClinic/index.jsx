import React from "react";
import PropTypes from "prop-types";
import { Header } from "../Header";
import { Title } from "../Title";
import { ClinicCard } from "../ClinicCard";
import { Link } from "../Link";
import { Button } from "../Button";
import { Divider } from "../Divider";

export const TreatmentsClinic = ({ clinics ,className, ...props }) => {
  return (
    <div {...props}
      className={[className, "min-h-screen bg-lightgray"]
        .filter(Boolean)
        .join(" ")}
    >
      <Header type="login" className="bg-white" />

      <section className="p-4 pb-0 shadow-inner">
        <div className="flex justify-center items-center">
          <Title
            font="bold"
            className="text-secondary text-2xl tracking-wider"
          >זימון תור לטיפול טוויגא</Title>
        </div>
        <div className="flex justify-center items-center gap-2 my-1">
          <Title
            font="regular"
            className="text-gunmetal text-md tracking-wide "
          >באיזור קרית אונו</Title>
          <div>ast</div>
        </div>
        <div className="flex justify-between items-start mt-12">
            <div className="flex-col flex">
                <Title
                  font="regular"
                  className="text-gunmetal tracking-wide"
                >{`נמצאו ${clinics?.length} `}</Title>
                <Title font="regular"
                  className="text-gunmetal tracking-wide">מרפאות</Title>
            </div>
          <div className="flex justify-center items-center">
            <Title
              font="regular"
              className="text-gunmetal tracking-wide text-sm"
            >
              מיין לפי:
            </Title>
            <div className="flex items-center">
              <Link underline="none" className="">מיקום</Link>
              <span className="font-open-sans-hb-regular text-gunmetal text-sm">|</span>
              <Link underline="none" className="">תאריך</Link>
            </div>
          </div>
        </div>
      </section>

      <section className="bg-white p-5 flex flex-col gap-6">
            {clinics && clinics.map((c,i)=><div><ClinicCard key={i} clinicObj={c} />{i!==clinics?.length-1 && <Divider className="mt-5" />}</div>)}
      </section>
    </div>
  );
};

TreatmentsClinic.propTypes = {
    clinics: PropTypes.array
};

TreatmentsClinic.defaultProps = {
    
};

import React from "react";
import PropTypes from "prop-types";

export const Tab = ({ selected, className, ...props }) => (
  <div {...props} className={["transition-all text-center duration-100 select-none text-primary text-xl font-open-sans-hb-regular border-2  rounded shadow-lg p-2", className, selected&&"bg-primary bg-opacity-5 hover:bg-opacity-20 font-open-sans-hb-bold border-primary"].filter(Boolean).join(" ")} />
);

Tab.propTypes = {
  children: PropTypes.string,
  selected: PropTypes.bool,
};

Tab.defaultProps = {

};

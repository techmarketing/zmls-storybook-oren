import React from 'react';

import { Tab } from '.';

export default {
  title: 'old/atoms/Tab',
  component: Tab,
  parameters: {
    backgrounds: {
      values: [
        { name: 'red', value: '#f00', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
  },
  args:{
    children: "כל התורים",
    selected: false,
  }
};

const Template = (args) => <Tab {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


import React, { useState } from "react";
import PropTypes from "prop-types";
import { Header } from "../Header";
import { Title } from "../Title";
import { Input } from "../Input";
import { Button } from "../Button";
import { Link } from "../Link";
import { CardContainer } from "../CardContainer";

export const LoginWithPhone = ({ className, ...props }) => {
  const [inputVal, setInputVal] = useState("");

  return (
    <div {...props}
      className={[className, "bg-lightgray h-screen overflow-auto pb-3"]
        .filter(Boolean)
        .join(" ")}
    >
      <Header loggedIn={false} className="" type={"login"} />
      <div className="shadow-inner px-4">
        <div className="flex justify-center items-center pt-3">
          <Title
            size="md"
            font="bold"
            className=" text-secondary text-3xl my-8 leading-10 text-center"
          >כניסה עם קוד חד פעמי ב-SMS</Title>
        </div>

        <div className="flex items-center mb-8">
          <Title
            titleType="subtitle-gray"
            size="sm"
            font="light"
            className="text-center leading-7 text-secondary text-xl "
          >שלחנו SMS עם קוד חד פעמי למספר
          טלפון נייד שמסתיים במספר 6847</Title>
        </div>

        <Input
          changeFoo={(e) => {
            setInputVal(e.target.value);
            console.log(e);
          }}
          className="w-full mt-3 py-1 px-6 font-open-sans-hb-regular tracking-wide text-lg"
          inputType="freedom"
          placeholder="יש להזין את הקוד שקיבלת "
        />

        <div className="flex justify-center items-center my-7">
          <Button
            type="submit"
            disabled={!inputVal}
            className=""
          >המשך</Button>
        </div>

        <CardContainer className="p-5 flex flex-col gap-2">
          <div className="flex">
            <div>ast</div>
            <Title
              className="text-gunmetal font-open-sans-hb-bold tracking-wide text-xl"
              titleType="blahblah"
              size="sm"
            >לא קיבלת את ה-SMS?</Title>
          </div>
          <div className="">
            <Link 
              className="ml-1"
            >יש ללחוץ כאן</Link>
            <Title
              font="light"
              className="text-gunmetal"
            >ונשלח אלייך שוב את הסיסמה</Title>
          </div>
        </CardContainer>
      </div>
    </div>
  );
};

LoginWithPhone.propTypes = {};

LoginWithPhone.defaultProps = {};

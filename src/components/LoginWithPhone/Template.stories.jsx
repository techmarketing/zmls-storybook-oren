import React from 'react';


import { LoginWithPhone } from '.';

export default {
  title: 'templates/LoginWithPhone',
  component: LoginWithPhone,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
};

const Template = (args) => <LoginWithPhone {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


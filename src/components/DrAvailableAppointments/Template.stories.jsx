import React from "react";

import { DrAvailableAppointments } from ".";

export default {
  title: "molecules/DrAvailableAppointments",
  component: DrAvailableAppointments,
  parameters: {
    backgrounds: {
      values: [
        { name: "fun gray", value: "#f7f9fc", default: true },
        { name: "green", value: "#0f0" },
        { name: "blue", value: "#00f" },
      ],
    },
  },
  argTypes: {},
  args: {
    dr: "ד״ר ירון",
    appointments: [
      { time: "09:50" },
      { time: "09:35" },
      { time: "10:30" },
      { time: "11:15" },
      { time: "12:00" },
      { time: "12:45" },
      { time: "16:00" },
      { time: "09:50" },
      { time: "09:50" },
      { time: "09:50" },
      { time: "09:50" },
      { time: "09:50" },
      { time: "09:50" },
      { time: "09:50" },
      { time: "09:50" },
    ],
  },
};

const Template = (args) => <DrAvailableAppointments {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  // label: 'Primary',
};

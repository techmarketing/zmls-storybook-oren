import React, { useState, useMemo, useEffect } from "react";
import PropTypes from "prop-types";
import { CardContainer } from "../CardContainer";
import { Title } from "../Title";
import { Radio } from "../Radio";
import { Link } from "../Link";
import { Button } from "../Button";

const DrAvailableAppointmentsPopup = () => {
  return (
    <div className="flex justify-center items-center mt-8">
      <Button
        classNames=""
      >זימון תור</Button>
    </div>
  );
};

export const DrAvailableAppointments = ({
  resetRadios,
  appointments,
  dr,
  clinic,
  address,
  className,
  ...props
}) => {
  // this boolean will determine if a load more link would show up and hide the last elements
  // the link will appear at the last position of the 2nd row of the radio grid, in place of the last radio.
  // clicking on it will show the ret of the radios
  const [loadMore, setLoadMore] = useState(false);
  const [chosenAppointment, setChosenAppointment] = useState(null);

  useEffect(() => {
    setChosenAppointment(null);
  }, [resetRadios]);

  return (
    <div {...props}
      className={[className, appointments.length === 0 && "hidden"]
        .filter(Boolean)
        .join(" ")}
    >
      <CardContainer className="border-2 border-very-light-blue py-4 px-2 pb-6">
        <div className="flex gap-5 ">
          <div className=" flex justify-end border-2 items-center">ast</div>
          <Title
            font="bold"
            className=" text-secondary text-xl"
          >{dr}</Title>
        </div>
        <div className="mt-2">
          <Title font="regular" className="text-gunmetal tracking-wide">מרפאת {clinic} - {address}</Title>
        </div>
        <div className="grid grid-cols-4 md:grid-cols-6 gap-2 px-3 mt-5">
          {appointments.map((a, i) => (
            <div
              key={i}
              className={`w-full flex justify-center items-center ${
                !loadMore && i >= 7 && "hidden"
              } ${!loadMore && i >= 11 && "md:hidden"}`}
            >
              <Radio
                onClick={() => setChosenAppointment(i)}
                className="w-full py-2"
                chosen={chosenAppointment === i}
                label={a.time}
              />
            </div>
          ))}
          {!loadMore && appointments.length > 7 && (
            <div className="w-full flex justify-center items-center">
              <Link
                onClick={() => setLoadMore(true)}
                color="black"
                className="text-lg"
              >טען עוד</Link>
            </div>
          )}
        </div>
        {chosenAppointment !== null && <DrAvailableAppointmentsPopup />}
      </CardContainer>
    </div>
  );
};

DrAvailableAppointments.propTypes = {
  dr: PropTypes.string,
  appointments: PropTypes.array,
};

DrAvailableAppointments.defaultProps = {
  
};

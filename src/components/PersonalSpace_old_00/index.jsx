import React from 'react'
import PropTypes from 'prop-types'
import { Title } from '../Title'
import { AvatarGallery2 } from '../AvatarGallery2'
import { Tab } from '../Tabs'

export const PersonalSpace_old_00 = ({className, ...props}) => {
    return (
        <div className={[
            "flex justify-center items-center flex-col gap-2  box-border",
            className
        ].filter(Boolean).join(" ")}>
            <Title className="text-3xl text-secondary" font="bold">אזור אישי</Title>
            <Title titleType="gunmetal" className={"mb-5 tracking-wide text-gunmetal"} font="regular" >דולב גרוזנברג</Title>
            <AvatarGallery2 className="w-1/2 md:w-1/2 flex justify-center items-center select-none"/>
            <div
            className={"flex flex-row-reverse mt-5 w-full"}
            >
                <Tab selected={true} className="text-md py-1" />
            </div>

        </div>
    )
}

PersonalSpace_old_00.propTypes = {
    
}

PersonalSpace_old_00.defaultProps = {

}
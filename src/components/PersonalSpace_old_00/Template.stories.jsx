import React from 'react';


import { PersonalSpace_old_00 } from '.';

export default {
  title: 'old/molecules/PersonalSpace_old_00',
  component: PersonalSpace_old_00,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
};

const Template = (args) => <PersonalSpace_old_00 {...args} />;

export const Primary = PersonalSpace_old_00.bind({});
Primary.args = {
    // label: 'Primary',
};


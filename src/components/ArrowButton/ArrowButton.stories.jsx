import React from 'react';

import { ArrowButton } from '.';

export default {
  title: 'atoms/ArrowButton',
  component: ArrowButton,
  parameters: {
    backgrounds: {
      values: [
        { name: 'red', value: '#f00' },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    ltn: {
        name: "Forward/Backward",
        control: { type: 'radio', options:[true, false] }
    },
    className: {
        name: 'CSS Classes',
        control: {type: 'text'}
    }
  },
};

const Template = (args) => <div>needast<ArrowButton {...args} /></div>;

export const Primary = Template.bind({});
Primary.args = {
    // date: Da
};


import React from 'react'
import PropTypes from 'prop-types'

import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';


export const ArrowButton = ({ltn , className,...props}) => {
    if(ltn){
        return <ArrowBackIosIcon {...props} className={[className, "font-thin"].filter(Boolean).join(" ")}/>
    }
    else {
        return <ArrowForwardIosIcon {...props} className={[className, "font-thin"].filter(Boolean).join(" ")}/>
    }
}

ArrowButton.propTypes = {
    ltn: PropTypes.bool
}

ArrowButton.defaultProps = {
    ltn: true
}


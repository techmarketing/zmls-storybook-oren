import React from 'react'
import PropTypes from 'prop-types'

export const Link = ({underline ,className , ...props}) => <a {...props} className={[
    "cursor-pointer select-none tracking-wide font-open-sans-hb-regular",
    !className&&"text-xl",
    underline==="hover" &&"hover:underline",
    underline==="always" &&"underline",
    className
].filter(Boolean).join(" ")} />

Link.propTypes = {
    children: PropTypes.string,
    href: PropTypes.string,
    underline: PropTypes.oneOf(["always", "hover", "none"]),
}

Link.defaultProps = {
    underline: "always",
}
import React from 'react';
import { Link } from '.';

export default {
  title: 'atoms/Link',
  component: Link,
  parameters: {
    backgrounds: {
      values: [
        { name: 'red', value: '#f00' },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
  },
  args: {
    children: 'סינון תוצאות',
    className: "",
    // underline: "always"
  }
};

const Template = (args) => <Link {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


import React from 'react';


import { Timer } from '.';

export default {
  title: 'atoms/Timer',
  component: Timer,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    startingTime:{
      type: {name:'string'}
    },
    endingTime:{
      type: {name:'string'}
    },
  },
};

const Template = (args) => <Timer {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};

const Template2 = (args) => <Timer startingTime="12:00" endingTime="02:00" />;

export const PlayWithStrings = Template2.bind({});
PlayWithStrings.args = {
    // label: 'Primary',
};


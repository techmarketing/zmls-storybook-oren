import React, { useEffect, useMemo, useState } from "react";

export const Timer = ({
  endingTime = new Date(new Date().setMinutes(new Date().getMinutes()+10)),
//   endingTime = "02:10",
  tick = 1000,
  startingTime = new Date(),
//   startingTime = "23:00",
  className,
  ...props
}) => {
  startingTime = useMemo(() => {
    if (typeof startingTime !== "string") {
      return new Date(startingTime)
    }
    const splitted = startingTime.split(":")
    const [h, m, s] = splitted;

    if(splitted.length===1){
        return new Date(2000, 0, 0, h, 0, 0);
    }
    if(splitted.length===2){
        return new Date(2000, 0, 0, h, m, 0);
    }
    if(splitted.length===3){
        return new Date(2000, 0, 0, h, m, s);
    }

  }, []);

  const timeToString = (time) => {
    const hours = Math.floor(time / 1000 / 60 / 60);
    const mins = Math.floor(time / 1000 / 60) % 60;
    const secs = Math.floor((time / 1000) % 60);
    const ms = time / 10
    if(endingTimeAsDate - startingTime > 1000*3600){
        if(determineTick(tick)<1000){
            return `${("0" + mins).slice(-2)}:${("0" + secs).slice(-2)}:${("0"+ms).slice(-2)}`;
        }
        return `${("0" + hours).slice(-2)}:${("0" + mins).slice(-2)}:${("0" + secs).slice(-2)}`;
    }
    if(determineTick(tick)<1000){
        return `${("0" + mins).slice(-2)}:${("0" + secs).slice(-2)}:${("0"+ms).slice(-2)}`;
    }
    return `${("0" + mins).slice(-2)}:${("0" + secs).slice(-2)}`;
};

  const determineTick = (string) => {
      if(typeof string === "number"){
          return string
      }

      // get s m and h
      const splitted = string.split(":")
      const [h, m, s] = splitted
      if(splitted.length===1){
        return h*1000
      }
      if(splitted.length===2){
        return h*1000*60 + m*1000
      }
      if(splitted.length===3){
        return h*1000*60*60 + m*1000*60 + s*1000
      }
      
  }

  const endingTimeAsDate = useMemo(() => {
    if (endingTime) {
      if (Object.prototype.toString.call(endingTime) === "[object Date]") {
        return endingTime;
      }
      const currH = startingTime.getHours()
      const splitted = endingTime.split(":");
      let date;
      let s;
      let m;
      let h;
      switch (splitted.length) {
        case 1:
          h = endingTime;
          date = new Date(
            startingTime.getFullYear(),
            startingTime.getMonth(),
            currH <= h ? startingTime.getDate() : startingTime.getDate() + 1,
            h,
            0,
            0
          );
          break;
        case 2:
          [h, m] = splitted;
          date = new Date(
            startingTime.getFullYear(),
            startingTime.getMonth(),
            currH <= h ? startingTime.getDate() : startingTime.getDate() + 1,
            h,
            m,
            0,
          );
          break;
        case 3:
          [h, m, s] = splitted;
          date = new Date(
            startingTime.getFullYear(),
            startingTime.getMonth(),
            currH <= h ? startingTime.getDate() : startingTime.getDate() + 1,
            h,
            m,
            s
          );
          break;
        default:
          break;
      }
      return date;
    }


    return new Date(
      startingTime.getFullYear(),
      startingTime.getMonth(),
      startingTime.getDate(),
      startingTime.getHours(),
      startingTime.getMinutes() + 10,
      startingTime.getSeconds()
    );
  }, [startingTime]);

  const [timeLeft, setTimeLeft] = useState(endingTimeAsDate - startingTime);

  useEffect(() => {
    if (timeLeft > 0) {
      const intervalID = setInterval(() => {
        setTimeLeft(timeLeft - determineTick(tick));
      }, determineTick(tick));
      return () => clearInterval(intervalID);
    }
  }, [timeLeft]);

  return (
    <div {...props} className={[className].filter(Boolean).join(" ")}>
      {timeLeft!==0 ? timeToString(timeLeft) : "Done"}
    </div>
  );
};

Timer.propTypes = {};

Timer.defaultProps = {};

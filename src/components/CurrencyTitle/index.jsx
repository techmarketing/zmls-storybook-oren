import React from 'react'
import PropTypes from 'prop-types'

export const CurrencyTitle = ({price ,className, ...props}) => {
    return (
        <div
        {...props}
        className={[
            'select-none font-open-sans-hb-regular',
        ].filter(Boolean).join(" ")}
        >
            ₪ <span className="text-5xl font-open-sans-hb-bold">{price}</span>
        </div>
    )
}

CurrencyTitle.propTypes = { 
    price: PropTypes.number,
}

CurrencyTitle.defaultProps = { 
}

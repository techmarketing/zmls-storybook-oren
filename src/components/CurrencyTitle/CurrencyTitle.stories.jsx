import React from 'react';

import { CurrencyTitle } from '.';

export default {
  title: 'atoms/CurrencyTitle',
  component: CurrencyTitle,
  parameters: {
    backgrounds: {
      values: [
        { name: 'red', value: '#f00', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    className: {
        name: "CSS Classes",
        control: {type: "text"}
    }
  },
  args: {
    price: 33,
  }
};

const Template = (args) => <CurrencyTitle {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


import React from "react";
import { Header } from "../Header";
import { Title } from "../Title";
import { CardContainer } from "../CardContainer";
import { Button } from "../Button";
import { Divider } from "../Divider";

export const SuccessfulAppoinment = ({ className, ...props }) => {
  return (
    <div {...props} className={[className].filter(Boolean).join(" ")}>
      <div className="bg-lightgray h-screen overflow-auto">
        <Header className="py-5 bg-white" type="login" />
        <div className="px-6">
          <div className="flex flex-col justify-between items-center">
            <div className="my-8">ast</div>
            <Title font="bold" className="text-subtitle-blue text-2xl">
              התור נקבע בהצלחה!
            </Title>
          </div>
          <CardContainer className="p-4 my-4 flex flex-col gap-3">
            <div className="flex flex-row text-right">
              <div className=" w-1/12"></div>
              <Title className="w-11/12 text-gunmetal" font="bold">
                ייעוץ רפואה משלימה
              </Title>
            </div>
            <div className="flex flex-row text-right">
              <div className=" w-1/12">ast</div>
              <Title className="w-11/12 text-subtitle-gray" font="regular">
                מרפאת אשקלון סנטר
              </Title>
            </div>
            <div className="flex flex-row text-right">
              <div className=" w-1/12">ast</div>
              <Title className="w-11/12 text-subtitle-gray" font="regular">
                גבעת השיקמה 2, קריית אונו
              </Title>
            </div>
            <div className="flex flex-row text-right">
              <div className=" w-1/12">ast</div>
              <Title className="w-11/12 text-subtitle-gray" font="regular">
                05.02.20 | יום ד׳
              </Title>
            </div>
            <div className="flex flex-row text-right">
              <div className=" w-1/12">ast</div>
              <Title className="w-11/12 text-subtitle-gray" font="regular">
                09:40
              </Title>
            </div>
            <div className="flex flex-row text-right">
              <div className=" w-1/12">ast</div>
              <Title className="w-11/12 text-subtitle-gray" font="regular">
                מטפל: ירושלמי בן הרוש שלומי
              </Title>
            </div>
            <Divider className="mb-5" />
            <div className="flex w-full justify-between gap-4">
              <Button classNames="w-1/2">
                סנכרון ליומן
              </Button>
              <Button
                classNames="w-1/2"
                style="outline"
              >
                חזרה לראשי
              </Button>
            </div>
          </CardContainer>
          <CardContainer className="p-4 flex flex-row">
            <div className=" w-1/12 text-right">ast</div>
            <Title
              className="w-11/12 text-right text-subtitle-gray"
              font="regular"
            >
              נא לא לשכוח להביא כרטיס מגנטי!
            </Title>
          </CardContainer>
        </div>
      </div>
    </div>
  );
};

SuccessfulAppoinment.propTypes = {};

SuccessfulAppoinment.defaultProps = {};

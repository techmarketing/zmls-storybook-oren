import React from 'react';


import { SuccessfulAppoinment } from '.';

export default {
  title: 'templates/SuccessfulAppoinment',
  component: SuccessfulAppoinment,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
};

const Template = (args) => <SuccessfulAppoinment {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


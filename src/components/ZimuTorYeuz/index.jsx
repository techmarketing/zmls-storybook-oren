import React, { useMemo, useState } from "react";
import PropTypes from "prop-types";
import { Header } from "../Header";
import { Title } from "../Title";
import { Link } from "../Link";
import { AvailableDatesGallery } from "../AvailableDatesGallery";
import { DrAvailableAppointments } from "../DrAvailableAppointments";
import { Tab } from "../Tabs";

export const ZimuTorYeuz = ({ availableAppointments, className, ...props }) => {
  //   .oooooo.     .oooooo.   ooo        ooooo ooooooooo.      ooooooooooooo   .oooooo.     .oooooo.   ooooo         .oooooo..o
  //  d8P'  `Y8b   d8P'  `Y8b  `88.       .888' `888   `Y88.    8'   888   `8  d8P'  `Y8b   d8P'  `Y8b  `888'        d8P'    `Y8
  // 888          888      888  888b     d'888   888   .d88'         888      888      888 888      888  888         Y88bo.
  // 888          888      888  8 Y88. .P  888   888ooo88P'          888      888      888 888      888  888          `"Y8888o.
  // 888          888      888  8  `888'   888   888                 888      888      888 888      888  888              `"Y88b
  // `88b    ooo  `88b    d88'  8    Y     888   888                 888      `88b    d88' `88b    d88'  888       o oo     .d8P
  //  `Y8bood8P'   `Y8bood8P'  o8o        o888o o888o               o888o      `Y8bood8P'   `Y8bood8P'  o888ooooood8 8""88888P'
  const tabsArr = [
    {
      label: "כל התורים",
      fatherFoo: () => console.log("changing tab"),
      switch: "all",
    },
    {
      label: "אונליין",
      fatherFoo: () => console.log("changing tab"),
      switch: "online",
    },
    {
      label: "במרפאה",
      fatherFoo: () => console.log("changing tab"),
      switch: "front",
    },
  ];

  //  .oooooo..o ooooooooooooo       .o.       ooooooooooooo oooooooooooo
  //  d8P'    `Y8 8'   888   `8      .888.      8'   888   `8 `888'     `8
  //  Y88bo.           888          .8"888.          888       888
  //   `"Y8888o.       888         .8' `888.         888       888oooo8
  //       `"Y88b      888        .88ooo8888.        888       888    "
  //  oo     .d8P      888       .8'     `888.       888       888       o
  //  8""88888P'      o888o     o88o     o8888o     o888o     o888ooooood8

  // proccess the object to an array of dates
  const datesArr = useMemo(() => {
    const datesArr = [new Date()];
    for (const dr in availableAppointments) {
      for (const appointment of availableAppointments[dr].appointments) {
        if (
          !datesArr.some(
            (d) =>
              d.getFullYear() === appointment.date.getFullYear() &&
              d.getMonth() === appointment.date.getMonth() &&
              d.getDate() === appointment.date.getDate()
            // &&
            // ((tabsArr[chosenTab]?.switch === "online" &&
            //   appointment.isOnline === true) ||
            //   (tabsArr[chosenTab]?.switch === "false" &&
            //     appointment.isOnline === false) ||
            //   tabsArr[chosenTab]?.switch === "all")
          )
        ) {
          datesArr.push(appointment.date);
        }
      }
    }
    return datesArr;
  }, [availableAppointments, currDate, chosenTab]);

  const [currDate, setCurrDate] = useState(datesArr[0]);
  const [resetRadios, setResetRadios] = useState(false);
  const [chosenTab, setChosenTab] = useState(0);

  // filter out the appointnments that are not of this date and are in the switch (online/front/all)
  const availableAppointmentsSum = useMemo(() => {
    let sum = 0;
    for (const dr in availableAppointments) {
      sum += availableAppointments[dr].appointments.filter(
        (a) =>
          a.date.getFullYear() === currDate.getFullYear() &&
          a.date.getMonth() === currDate.getMonth() &&
          a.date.getDate() === currDate.getDate() &&
          ((tabsArr[chosenTab]?.switch === "online" && a.isOnline === true) ||
            (tabsArr[chosenTab]?.switch === "front" && a.isOnline === false) ||
            tabsArr[chosenTab]?.switch === "all")
      ).length;
    }
    return sum;
  }, [availableAppointments, currDate, chosenTab]);

  // ooooo   ooooo   .oooooo.     .oooooo.   oooo    oooo  .oooooo..o
  // `888'   `888'  d8P'  `Y8b   d8P'  `Y8b  `888   .8P'  d8P'    `Y8
  //  888     888  888      888 888      888  888  d8'    Y88bo.
  //  888ooooo888  888      888 888      888  88888[       `"Y8888o.
  //  888     888  888      888 888      888  888`88b.         `"Y88b
  //  888     888  `88b    d88' `88b    d88'  888  `88b.  oo     .d8P
  // o888o   o888o  `Y8bood8P'   `Y8bood8P'  o888o  o888o 8""88888P'

  return (
    <div
      {...props}
      className={[className, "bg-lightgray min-h-screen"]
        .filter(Boolean)
        .join(" ")}
    >
      <Header
        loggedIn={true}
        backBtn={true}
        type="login"
        className="bg-white py-5"
        logoWidth="w-1/3"
      />

      <section className="p-4 shadow-inner">
        <div className="px-2">
          <div className="flex justify-center mb-4 mt-1 items-center">
            <Title
              font="bold"
              className="text-secondary text-3xl tracking-wider"
            >
              זימון תור לייעוץ
            </Title>
          </div>
          <div className="flex justify-center items-center gap-2 my-2">
            <Title
              font="regular"
              className="text-gunmetal text-xl tracking-wide "
            >
              באיזור קרית אונו
            </Title>
            <div>ast</div>
          </div>
        </div>

        <div className="flex justify-between w-full gap-3 mt-7">
          {tabsArr.map((t, i) => (
            <Tab
              selected={i === chosenTab}
              onClick={() => setChosenTab(i)}
              children={t.label}
              fatherFoo={t.fatherFoo}
              className="w-1/3 min-w-max"
            />
          ))}
        </div>

        <div className="flex flex-col gap-4 mt-5">
          <div className="px-2">
            <Title
              className="text-xl tracking-wide underline text-primary"
              font="regular"
            >
              תאריכים זמינים
            </Title>
          </div>
          <AvailableDatesGallery
            resetRadios={resetRadios}
            setResetRadios={setResetRadios}
            setCurrDate={setCurrDate}
            availableDates={datesArr}
          />
        </div>
      </section>

      <section className="pb-3 px-3 ">
        <div className="flex justify-between items-center px-2">
          <Title
            font="regular"
            className="text-secondary text-xl"
          >{`נמצאו ${availableAppointmentsSum} תורים:`}</Title>
          <Link color="green" className="font-open-sans-hb-regular text-xl">
            סינון תוצאות
          </Link>
        </div>

        <div className="flex flex-col gap-7 py-3">
          {Object.keys(availableAppointments).map((dr) => (
            <DrAvailableAppointments
              resetRadios={resetRadios}
              dr={dr}
              clinic={availableAppointments[dr].clinic}
              address={availableAppointments[dr].address}
              appointments={availableAppointments[dr].appointments.filter(
                (a) =>
                  a.date.getFullYear() === currDate.getFullYear() &&
                  a.date.getMonth() === currDate.getMonth() &&
                  a.date.getDate() === currDate.getDate() &&
                  ((tabsArr[chosenTab]?.switch === "online" &&
                    a.isOnline === true) ||
                    (tabsArr[chosenTab]?.switch === "front" &&
                      a.isOnline === false) ||
                    tabsArr[chosenTab]?.switch === "all")
              )}
            />
          ))}
        </div>
      </section>
    </div>
  );
};

ZimuTorYeuz.propTypes = {
  availableAppointments: PropTypes.object,
};

ZimuTorYeuz.defaultProps = {};

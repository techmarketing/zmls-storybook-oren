import React from 'react';


import { ZimuTorYeuz } from '.';

export default {
  title: 'templates/ZimuTorYeuz',
  component: ZimuTorYeuz,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
  args: {
    availableAppointments: {
      ["ד״ר בן-דוד שלמה"]: {
        clinic: "קיראון",
        address: "גבעת השקמה 2, קרית אונו",
        appointments:[
        { time: "09:35", isOnline: false ,date: new Date() },
        { time: "09:50", isOnline: false ,date: new Date() },
        { time: "10:30", isOnline: true ,date: new Date() },
        { time: "11:15", isOnline: false ,date: new Date() },
        { time: "09:35", isOnline: false ,date: new Date() },
        { time: "09:50", isOnline: false ,date: new Date() },
        { time: "10:30", isOnline: true ,date: new Date() },
        { time: "11:15", isOnline: false ,date: new Date() },
        { time: "09:35", isOnline: false ,date: new Date() },
        { time: "09:50", isOnline: false ,date: new Date() },
        { time: "10:30", isOnline: false ,date: new Date() },
        { time: "11:15", isOnline: true ,date: new Date() },
      ]},
      ["ד״ר אלטרמן מעיין"]: {
        clinic: "בת ים-בתימון",
        address: "הרב ניסנבאום 33, בת ים",
        appointments:[
        { time: "09:50",   isOnline: false ,date: new Date() },
        { time: "09:50",   isOnline: false ,date: new Date() },
        { time: "09:50",   isOnline: false ,date: new Date() },
        { time: "09:50",   isOnline: true ,date: new Date() },
        { time: "09:35",   isOnline: false ,date: new Date(2021, 11, 4) },
        { time: "10:30",   isOnline: false ,date: new Date(2021, 11, 4) },
        { time: "12:15",   isOnline: false ,date: new Date(2021, 11, 4) },
        { time: "09:50",   isOnline: true ,date: new Date(2021, 11, 4) },
      ]},
      ["ד״ר טל"]: {
        clinic: "טקמרקטינג",
        address: "ליד גשר קלקא",
        appointments:[
        { time: "09:50",  isOnline: false ,date: new Date() },
        { time: "09:50",  isOnline: true ,date: new Date() },
        { time: "09:50",  isOnline: false ,date: new Date() },
        { time: "09:50",  isOnline: true ,date: new Date() },
        { time: "09:35",  isOnline: false ,date: new Date(2022, 11, 5) },
        { time: "10:30",  isOnline: false ,date: new Date(2022, 9, 4) },
        { time: "12:15",  isOnline: false ,date: new Date(2022, 9, 5) },
        { time: "09:50",  isOnline: true ,date: new Date(2022, 9, 6) },
      ]},
      ["ד״ר אבי שפירו"]: {
        clinic: "מכבי",
        address: "גבעתיים",
        appointments:[
        { time: "09:50",   isOnline: false ,date: new Date() },
        { time: "09:50",   isOnline: true ,date: new Date() },
        { time: "09:50",   isOnline: false ,date: new Date() },
        { time: "09:50",   isOnline: true ,date: new Date() },
        { time: "09:35",   isOnline: false ,date: new Date(2022, 9, 7) },
        { time: "10:30",   isOnline: false ,date: new Date(2022, 9, 8) },
        { time: "12:15",   isOnline: false ,date: new Date(2022, 9, 9) },
        { time: "09:50",   isOnline: false ,date: new Date(2022, 9, 4) },
      ]},
      ["ד״ר רועי"]: {
        clinic: "טקמרקטינג",
        address: "בטקמרקטינג",
        appointments:[
        { time: "09:50", isOnline: false ,date: new Date() },
        { time: "09:50", isOnline: true ,date: new Date() },
        { time: "09:50", isOnline: false ,date: new Date() },
        { time: "09:50", isOnline: false ,date: new Date() },
        { time: "09:35", isOnline: true ,date: new Date(2022, 9, 1) },
        { time: "10:30", isOnline: false ,date: new Date(2022, 9, 2) },
        { time: "12:15", isOnline: true ,date: new Date(2022, 9, 5) },
        { time: "09:50", isOnline: false ,date: new Date(2022, 9, 22) },
      ]},
      ["ד״ר אורי"]: {
        clinic: "טקמרקטינג",
        address: "מול אבי שפירו",
        appointments:[
        { time: "09:50", isOnline: false ,date: new Date() },
        { time: "09:50", isOnline: true ,date: new Date() },
        { time: "09:50", isOnline: false ,date: new Date() },
        { time: "09:50", isOnline: false ,date: new Date() },
        { time: "09:35", isOnline: true ,date: new Date(2022, 9, 1) },
        { time: "10:30", isOnline: false ,date: new Date(2022, 9, 2) },
        { time: "12:15", isOnline: true ,date: new Date(2022, 9, 5) },
        { time: "09:50", isOnline: false ,date: new Date(2022, 9, 22) },
      ]},
    },
  }
};


// clinic: "קיראון", address: "", 
// clinic: "קיראון", address: "", 
// clinic: "קיראון", address: "", 
// clinic: "קיראון", address: "", 
// clinic: "קיראון", address: "", 
// clinic: "קיראון", address: "", 
// clinic: "קיראון", address: "", 
// clinic: "קיראון", address: "", 
// clinic: "קיראון", address: "", 
// clinic: "קיראון", address: "", 
// clinic: "קיראון", address: "", 
// clinic: "קיראון", address: "", 
// clinic: "בת ים-בתימון", address:
// clinic: "בת ים-בתימון", address:
// clinic: "בת ים-בתימון", address:
// clinic: "בת ים-בתימון", address:
// clinic: "בת ים-בתימון", address:
// clinic: "בת ים-בתימון", address:
// clinic: "בת ים-בתימון", address:
// clinic: "בת ים-בתימון", address:
// clinic: "טק-מרקטינג", address: 
// clinic: "טק-מרקטינג", address: 
// clinic: "טק-מרקטינג", address: 
// clinic: "טק-מרקטינג", address: 
// clinic: "טק-מרקטינג", address: 
// clinic: "טק-מרקטינג", address: 
// clinic: "טק-מרקטינג", address: 
// clinic: "טק-מרקטינג", address: 
// clinic: "מכבי תל-אביב", address:
// clinic: "מכבי תל-אביב", address:
// clinic: "מכבי תל-אביב", address:
// clinic: "מכבי תל-אביב", address:
// clinic: "מכבי תל-אביב", address:
// clinic: "מכבי תל-אביב", address:
// clinic: "מכבי תל-אביב", address:
// clinic: "מכבי תל-אביב", address:
// clinic: "פסיכיאטרית", address: "", 
// clinic: "פסיכיאטרית", address: "", 
// clinic: "פסיכיאטרית", address: "", 
// clinic: "פסיכיאטרית", address: "", 
// clinic: "פסיכיאטרית", address: "", 
// clinic: "פסיכיאטרית", address: "", 
// clinic: "פסיכיאטרית", address: "", 
// clinic: "פסיכיאטרית", address: "", 

const Template = (args) => <ZimuTorYeuz {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


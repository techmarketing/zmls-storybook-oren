import React, { useEffect, useState } from "react";
import { Title } from "../Title";
import { CardContainer } from "../CardContainer";
import { Header } from "../Header";
import { PersonalSpace } from "../PersonalSpace";
import { debounce } from "lodash";
import { NextAppointmentsMolecule } from "../NextAppointmentsMolecule";
import { Button } from "../Button";

export const PersonalAreaFull = ({ profilesArr ,className, ...props }) => {

    const [currProfileAppointments, setCurrProfileAppointments] = useState([])

    const fetchCurrProfileAppointments = async () => {
        const res = await fetch(`https://random-data-api.com/api/beer/random_beer?size=${Math.floor(Math.random()*21)}`)
        const data = await res.json()
        setCurrProfileAppointments(data)
    }

    const _fetchCurrProfileAppointments = debounce(fetchCurrProfileAppointments, 300)

    useEffect(() => {
        fetchCurrProfileAppointments()
    }, [])

  return (
    <div {...props} className={["bg-lightgray min-h-screen overflow-auto"].filter(Boolean).join(" ")}>
      <Header  logoWidth="w-[30%]" loggedIn={true} backBtn={false} className="py-5 bg-white" type="login" />
      <div className="shadow-inner py-6 px-7">
        <PersonalSpace fatherFoo={_fetchCurrProfileAppointments} profilesArr={profilesArr} />
        <Button style="outline-w-color" classNames="my-5 font-open-sans-hb-bold">כל התורים</Button>
        <NextAppointmentsMolecule appointmentsArr={currProfileAppointments} />
        <div className="mt-5">
          <Title
            className="mt-5 text-subtitle-blue tracking-wide"
            font="regular"
          >
            טיפולים מאושרים לזימון תור:
          </Title>
        </div>
        <CardContainer className={"mt-2 p-6"}>
          <Title className="text-subtitle-gray tracking-wider" font="regular">
            לא נמצאו טיפולים מאושרים לזימון תור. יש לבצע ייעוץ עם רופא/ה תחילה
          </Title>
        </CardContainer>
      </div>
    </div>
  );
};

PersonalAreaFull.propTypes = {};

PersonalAreaFull.defaultProps = {};

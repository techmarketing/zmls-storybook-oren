import React from "react";
import PropTypes from "prop-types";
import { CardContainer } from "../CardContainer";
import { Title } from "../Title";
import CloseIcon from "@material-ui/icons/Close";

export const Notification2Mins_1 = ({ className, ...props }) => {
  return (
    <CardContainer
      {...props}
      className={[className, "relative pb-6"].filter(Boolean).join(" ")}
    >
      <CloseIcon className="absolute right-5 top-5 text-gunmetal" />
      <div className="flex justify-center items-center h-24 text-3xl">ast</div>
      <div className="flex justify-center items-center">
        <Title className="text-center text-secondary text-2xl" font="bold">התור שלך יישמר לעוד שתי דקות</Title>
      </div>
      <div className="flex justify-center items-center">
        <Title className="text-gunmetal text-center tracking-wide mt-3" font="regular">על מנת להבטיח את התור שביקשת, מומלץ להזדרז ולהזמין</Title>
      </div>
    </CardContainer>
  );
};

Notification2Mins_1.propTypes = {};

Notification2Mins_1.defaultProps = {};

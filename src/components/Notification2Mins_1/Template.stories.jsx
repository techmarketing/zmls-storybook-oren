import React from 'react';


import { Notification2Mins_1 } from '.';

export default {
  title: 'molecules/Notification2Mins_1',
  component: Notification2Mins_1,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
};

const Template = (args) => <div className="h-screen p-4 bg-lightgray"><Notification2Mins_1 {...args} /></div>;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


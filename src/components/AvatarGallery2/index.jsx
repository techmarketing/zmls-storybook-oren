import React, { useState, useCallback, useMemo, useEffect } from "react";
import PropTypes from "prop-types";
import { ArrowButton } from "../ArrowButton";
import PersonalSpaceAvatar, { supportedColors } from "../PeronsalSpaceAvatar";
import useGalleryItemStyling from '../../useGalleryItemStyling'

export const AvatarGallery2 = ({ className ,profilesArr, maxItems }) => {
  const [currIndex, setCurrIndex] = useState(2)

  const calculateTransform = useGalleryItemStyling(maxItems, [.6, .4], .8);

  return (
    <div className={["flex items-center h-16 lg:h-24", className].filter(Boolean).join(" ")}>
        <ArrowButton
          className={["z-20 text-gunmetal cursor-pointer", currIndex===profilesArr.length-1&&"opacity-0 pointer-events-none "].filter(Boolean).join(" ")}
          onClick={()=>{
            setCurrIndex(currIndex+1)
          }}
          ltn={false}/>
      <div className={"flex relative flex-grow justify-center items-center"}>
        {profilesArr.map((p, index)=><div className="absolute" 
        style={{
          transitionDuration: '.3s',
          transitionProperty: 'transform opacity',
          ...calculateTransform(index-currIndex)
        }}><PersonalSpaceAvatar
          key={p.id}
          colorIndex={
            index % supportedColors.length
          }
          current={index===currIndex}
          className="transition-all  opacity-1 ease-in-out transform flex justify-center items-center shadow select-none"
          >{p.fname} {index-currIndex}</PersonalSpaceAvatar></div>)}
      </div>
          <ArrowButton
            className={["z-20 cursor-pointer text-gunmetal", currIndex===0&&"opacity-0 pointer-events-none" ].filter(Boolean).join(" ")}
            onClick={()=>{
              setCurrIndex(currIndex-1)
            }}
            />
    </div>
  );
};

AvatarGallery2.propTypes = {
  profilesArr: PropTypes.array.isRequired,
  className: PropTypes.string,
  maxItems: PropTypes.number
  
};

AvatarGallery2.defaultProps = {
  
};

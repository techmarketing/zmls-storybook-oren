import React from 'react';


import { AvatarGallery2 } from '.';

export default {
  title: 'molecules/AvatarGallery2',
  component: AvatarGallery2,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
  args: {
    maxItems: 3,
  profilesArr: [
    { fname: "דולב", lname: "גרוזנברג", id: "dolev_id" },
    { fname: "יוני", lname: "יוניהו", id: "yoni_id" },
    { fname: "קרן", lname: "ישראלשווילי", id: "israel_id" },
    { fname: "מאיה", lname: "ישראלשווילי", id: "sami_id" },
    { fname: "עופר", lname: "ישראלשווילי", id: "ofer_id" },

    { fname: "עופר", lname: "ישראלשווילי", id: "1" },

    { fname: "עופר", lname: "ישראלשווילי", id: "2" },

    { fname: "עופר", lname: "ישראלשווילי", id: "3" },

    { fname: "עופר", lname: "ישראלשווילי", id: "4" },
    { fname: "עופר", lname: "ישראלשווילי", id: "5" },
    { fname: "עופר", lname: "ישראלשווילי", id: "6" },
    { fname: "עופר", lname: "ישראלשווילי", id: "7" },
  ],
  className: ""
  }
};

const Template = (args) => <div><p>Welcome here</p><AvatarGallery2 {...args} /></div>;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


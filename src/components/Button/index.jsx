import React from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress';


export const Button = ({ loading ,color ,style , classNames, rounded, ...props}) => (
    <button
        {...props}
        className={[
            "px-4 py-2 font-open-sans-hb-regular transition-all ease-in-out duration-500 tracking-wider flex justify-center items-center box-border border-2",
            style==="full" && !props.disabled  && ` text-white ${color==="primary" && 'bg-primary border-primary'} ${color==="error" && 'bg-error border-error'} hover:opacity-80`,
            style==="outline" && !props.disabled  && `${color==="primary" && "text-primary"} ${color==="error" && "text-error"} bg-white  ${color==="primary" && "border-primary"} ${color==="error" && "border-error"}`,
            style==="outline-w-color" && !props.disabled  && `${color==="primary" && "text-primary"} ${color==="error" && "text-error"} ${color==="primary" && "bg-primary"} ${color==="error" && "text-error"}  ${color==="primary" && "border-primary"} ${color==="error" && "border-error"} bg-opacity-5`,
            style==="naked" && !props.disabled  && `bg-transparent border-transparent ${color==="primary" && "text-primary"} ${color==="error" && "text-error"}`,
            rounded ? 'rounded-full' : 'rounded',
            props.disabled && "cursor-not-allowed bg-error text-white border-error",
            loading && " opacity-40 cursor-wait",
            classNames,
         ].filter(Boolean).join(' ')} >
             <div className="ml-1 flex justify-center items-center">
                {loading && <CircularProgress color="white" size="1rem"  />}
             </div>
             {props.children}
         </button>
)


Button.propTypes = {
    children: PropTypes.string,
    type: PropTypes.oneOf(['button', 'submit', 'reset']),
    disabled: PropTypes.bool,
    style: PropTypes.oneOf(['outline', 'naked', 'full', 'outline-w-color']),
    color: PropTypes.oneOf(['primary', 'error']),
    rounded: PropTypes.bool,
    loading: PropTypes.bool,
}

Button.defaultProps ={
    style: "full",
    color: "primary"
}
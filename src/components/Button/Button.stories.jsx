import React from 'react';

import { Button } from '.';

export default {
  title: 'atoms/Button',
  component: Button,
  parameters: {
    backgrounds: {
      values: [
        { name: 'red', value: '#f00' },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    children: {
        name: "Label",
        control: { type: 'text' }
    },
    className: {
        name: 'CSS Classes',
        control: {type: 'text'}
    }
  },
  args: {
    children: 'Primary',
    type: 'submit',
    disabled: false,
    rounded: false,
    style: "full",
    loading: false,
  }
};

const Template = (args) => <Button {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    children: 'Primary',
};

export const Outline = Template.bind({});
Outline.args = {
    children: 'Outline',
    style: "outline",
};

export const OutlineColored = Template.bind({});
OutlineColored.args = {
    children: 'Outline Colored',
    style: "outline-w-color",
};

export const Disabled = Template.bind({});
Disabled.args = {
    children: 'Disabled',
    disabled: true,
};

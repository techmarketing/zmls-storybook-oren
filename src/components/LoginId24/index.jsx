import React, { useState } from "react";
import { Header } from "../Header";
import { Title } from "../Title";
import { Input } from "../Input";
import { Button } from "../Button";

export const LoginId24 = ({ className, ...props }) => {

    const [inputVal, setInputVal] = useState("")

  return (
    <div {...props}
      className={[className, "min-h-screen bg-lightgray"]
        .filter(Boolean)
        .join(" ")}
    >
      <Header
        logoWidth="w-1/3"
        type="login"
        loggedIn={false}
        className="p-3 pb-4"
      />
      <section className="shadow-inner flex flex-col items-center px-2">
        <Title
          font="bold"
          className="text-secondary text-3xl text-center w-full mt-12"
        >
          {" "}
          ברוכים הבאים לאזור האישי בכללית רפואה משלימה
        </Title>
        <Title
          font="regular"
          className="text-secondary text-xl text-center w-full my-12"
        >
          לכניסה למערכת באמצעות קוד חד פעמי ב-SMS יש להזין את מספר תעודת הזהות
        </Title>
        <Input
          changeFoo={(e)=>setInputVal(e.target.value)}
          className="w-full py-2 px-6 font-open-sans-hb-regular text-xl text-gunmetal"
          inputType="id"
          wrapperWidth="w-full"
        />
        <div className="border-2 py-4 px-6 bg-gray-50 shadow-md rounded my-6 w-full ">
          SOME CAPTCHA
        </div>
        <Button
          disabled={!inputVal}
          classNames="w-2/4 min-w-12"
        >שלחו לי קוד לנייד</Button>
      </section>
    </div>
  );
};

LoginId24.propTypes = {};

LoginId24.defaultProps = {};

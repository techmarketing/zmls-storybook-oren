import React from 'react';


import { LoginId24 } from '.';

export default {
  title: 'templates/LoginId24',
  component: LoginId24,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
};

const Template = (args) => <LoginId24 {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


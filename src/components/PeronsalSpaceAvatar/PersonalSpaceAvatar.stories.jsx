import React from 'react';

import PersonalSpaceAvatar, { supportedColors } from '.';

export default {
  title: 'atoms/PersonalSpaceAvatar',
  component: PersonalSpaceAvatar,
  parameters: {
    backgrounds: {
      values: [
        { name: 'red', value: '#f00' },
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    className: {
        name: 'CSS Classes',
        control: {type: 'text'}
    },
  },
  args: {
    colorIndex: 0,
    current: true
  }
};

const Template = (args) => <PersonalSpaceAvatar {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    children: 'SM',
};
import React from 'react'
import PropTypes from 'prop-types'

export const supportedColors = ['bg-green-500', 'bg-blue-500', 'bg-yellow-500'];

const PersonalSpaceAvatar = ({ className, current, colorIndex, children, ...props}) => {
    return (
        <div 
        {...props}
        className={[
            "rounded-full flex items-center relative justify-center shadow w-20 h-20",
            className,
            supportedColors[colorIndex],
        ].filter(Boolean).join(" ")} >
            <span>{children}</span>
            <div className={`transition-opacity duration-300 rounded-full absolute inset-0 bg-white pointer-events-none ${current ? 'opacity-0 ' : 'opacity-60'}`} />
        </div>
    )
}

export default PersonalSpaceAvatar;

PersonalSpaceAvatar.propTypes = {
    color: PropTypes.oneOf(supportedColors),
    current: PropTypes.bool,

}

PersonalSpaceAvatar.defaultProps ={
    
}

import React from 'react';


import { SavedAppointment } from '.';

export default {
  title: 'templates/SavedAppointment',
  component: SavedAppointment,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
};

const Template = (args) => <SavedAppointment {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


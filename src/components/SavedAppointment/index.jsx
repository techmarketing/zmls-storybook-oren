import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Header } from "../Header";
import { Title } from "../Title";
import { numToString } from '../../functions/timeStringNumTranslators'

export const SavedAppointment = ({ reservalTime = new Date().toString().split(" ")[4]  ,className, ...props }) => {



    // const [timerVal, setTimerVal] = useState(numToString(reservalTime.getTime()/1000 - reservalTime.setMinutes(reservalTime+10)/1000))
    const [timerVal, setTimerVal] = useState(reservalTime)
    
    useEffect(() => {
      console.log(timerVal)     
      console.log()
    }, [])

  return (
    <div
      {...props}
      className={[className, "h-screen bg-lightgray"].filter(Boolean).join(" ")}
    >
      <Header
        loggedIn={true}
        backBtn={true}
        type="login"
        className="bg-white py-5"
        logoWidth="w-2/5 max-w-22"
      />

      <section className="shadow-inner px-4">
          <div className="flex justify-center items-center py-5">
            <Title font="bold" className="text-center text-3xl text-secondary">התור נשמר בהצלחה!</Title>
          </div>
          <div className="flex justify-start items-center">
            <Title font="regular" className="text-xl text-secondary">התור שנבחר יישמר עבורך ל-10 דקות עד
להשלמת תהליך התשלום.</Title>
          </div>
          <div className="flex justify-start items-center">
            <Title font="regular" className="text-xl text-secondary">לאחר מכן התור ישוחרר</Title>
          </div>
          <div className="flex justify-center items-center">
            <Title font="light" className="text-center text-xl text-coral">נותרו {timerVal} דקות לסיום שמירת הזימון</Title>
          </div>
      </section>


    </div>
  );
};

SavedAppointment.propTypes = {};

SavedAppointment.defaultProps = {};

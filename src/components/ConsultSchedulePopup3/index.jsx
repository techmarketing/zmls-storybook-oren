import React from "react";
import { CardContainer } from "../CardContainer";
import { Title } from "../Title";
import { Input } from "../Input";
import { Button } from "../Button";
import CloseIcon from "@material-ui/icons/Close";

export const ConsultSchedulePopup3 = ({ className, ...props }) => {
  return (
    <div {...props} className={[className].filter(Boolean).join(" ")}>
      <CardContainer>
        <div>
          <CloseIcon className="text-gunmetal" />
        </div>
        <div className="flex justify-center items-center text-3xl">
          <Title font="bold" className="text-secondary">
            מיקום התור
          </Title>
        </div>
        <div className="flex justify-center items-center text-lg my-5">
          <Title font="regular" className="text-secondary">
            יש להקליד את שם היישוב הנבחר
          </Title>
        </div>
        <Input
          className="my-12 px-4 py-2 font-open-sans-hb-regular tracking-wide"
          inputType="search"
          value="קרית אונו"
        />
        <div className="flex justify-center items-center mb-4">
          <Button
            style="outline-w-color"
            classNames=""
          >
            אישור
          </Button>
        </div>
      </CardContainer>
    </div>
  );
};

ConsultSchedulePopup3.propTypes = {};

ConsultSchedulePopup3.defaultProps = {};

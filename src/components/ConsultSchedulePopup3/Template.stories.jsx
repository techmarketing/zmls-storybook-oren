import React from 'react';


import { ConsultSchedulePopup3 } from '.';

export default {
  title: 'molecules/ConsultSchedulePopup3',
  component: ConsultSchedulePopup3,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
};

const Template = (args) => <ConsultSchedulePopup3 {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


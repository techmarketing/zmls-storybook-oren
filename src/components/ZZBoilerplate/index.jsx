import React from 'react'
import PropTypes from 'prop-types'

export const Boilerplate = ({className, ...props}) => {
    return (
        <div
        {...props}
        className={[
            className
        ].filter(Boolean).join(" ")}
        >
            Boilerplate works!
        </div>
    )
}

Boilerplate.propTypes = {
    
}

Boilerplate.defaultProps = {

}
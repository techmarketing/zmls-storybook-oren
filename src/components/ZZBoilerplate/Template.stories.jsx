import React from 'react';


import { Boilerplate } from '.';

export default {
  title: 'tools/Boilerplate',
  component: Boilerplate,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
};

const Template = (args) => <Boilerplate {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


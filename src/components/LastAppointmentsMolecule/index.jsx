import React from "react";
import PropTypes from "prop-types";
import { Title } from "../Title";
import { LastAppointmentCard } from "../LastAppointmentCard";

export const LastAppointmentsMolecule = ({
  appointmentsArr,
  className,
  ...props
}) => {
  return (
    <div {...props} className={[className].filter(Boolean).join(" ")}>
      <Title font="regular" className="text-subtitle-blue tracking-wider" >{`הטיפולים האחרונים שלי:`}</Title>
      <div
      
        className={["overflow-x-scroll pb-3"].filter(Boolean).join(" ")}
      >
        <div className="flex gap-3 w-max">
          {appointmentsArr?.map((a,i) => (
            <LastAppointmentCard key={i} appointment={a} />
          ))}
        </div>
      </div>
    </div>
  );
};

LastAppointmentsMolecule.propTypes = {
  appointmentsArr: PropTypes.array,
};

LastAppointmentsMolecule.defaultProps = {
  
};

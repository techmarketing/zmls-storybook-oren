import React from 'react';


import { LastAppointmentsMolecule } from '.';

export default {
  title: 'molecules/LastAppointmentsMolecule',
  component: LastAppointmentsMolecule,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
  args: {
    appointmentsArr: [
      {
        tipulName: "דיקור סיני אנטי אייג׳ינג",
        dr: "ירושלמי בן הרוש שלומית",
        clinic: "מרפאת קיראון",
      },
      {
        tipulName: "דיקור סיני אנטי אייג׳ינג",
        dr: "ירושלמי בן הרוש שלומית",
        clinic: "מרפאת קיראון",
      },
      {
        tipulName: "דיקור סיני אנטי אייג׳ינג",
        dr: "ירושלמי בן הרוש שלומית",
        clinic: "מרפאת קיראון",
      },
    ],
  }
};

const Template = (args) => <LastAppointmentsMolecule {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


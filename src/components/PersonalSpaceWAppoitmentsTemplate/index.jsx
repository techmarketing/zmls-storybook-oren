import React from 'react'
import { Header } from '../Header'
import { PersonalSpace_old_00 } from '../PersonalSpace_old_00'
import { TipulimMeusharimMolecule } from '../TipulimMeusharimMolecule'

export const PersonalSpace_old_01 = ({className, ...props}) => {
    return (
        <div {...props} className="bg-lightgray h-screen overflow-auto">
            <Header className="py-5 bg-white" type="login"/>
            <PersonalSpace_old_00 className="bg-white pt-5 shadow-inner" />
            <TipulimMeusharimMolecule className="shadow-inner pt-5 px-4" />
        </div>
    )
}

PersonalSpace_old_01.propTypes = {
}

PersonalSpace_old_01.defaultProps = {
}
import React from 'react';


import { PersonalSpace_old_01 } from '.';

export default {
  title: 'old/templates/PersonalSpace_old_01',
  component: PersonalSpace_old_01,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
};

const Template = (args) => <PersonalSpace_old_01 {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


import React from 'react';


import { AvailableDatesGallery } from '.';

export default {
  title: 'molecules/AvailableDatesGallery',
  component: AvailableDatesGallery,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {

  },
  args: {availableDates: [
    new Date(),
    new Date(),
    new Date(),
    new Date(),
    new Date(),
    new Date(),
    new Date(),
  ],}
};

const Template = (args) => <AvailableDatesGallery {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


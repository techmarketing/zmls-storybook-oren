import React, { useState } from "react";
import PropTypes from "prop-types";
import { AvailableDateCard } from "../AvailableDateCard";
import { ArrowButton } from "../ArrowButton";

export const AvailableDatesGallery = ({
  setResetRadios,
  resetRadios,
  setCurrDate,
  availableDates,
  className,
  ...props
}) => {


    
  const [chosenOne, setchosenOne] = useState(0)
  const [currIndex, setCurrIndex] = useState(0)

  return (
    <div {...props}
      className={[className, "flex justify-center items-center relative"]
        .filter(Boolean)
        .join(" ")}
    >
        <ArrowButton onClick={()=>setCurrIndex(currIndex+3)} className={["absolute left-0", currIndex+2>=availableDates?.length-1&&'opacity-0 pointer-events-none '].filter(Boolean).join(" ")} />
        <div className="flex gap-3 justify-between flex-row items-center px-5">
          {availableDates?.map((d,i)=><AvailableDateCard active={i===chosenOne} className={[i!==currIndex && i!==currIndex+1 && i!==currIndex+2 && 'hidden', "py-2 px-4"].filter(Boolean).join(" ")} onClick={()=>{setchosenOne(i);setCurrDate(d);setResetRadios(!resetRadios)}} date={d} />)}
        </div>
        <ArrowButton onClick={()=>setCurrIndex(currIndex-3)} className={["absolute right-0", currIndex===0&&'opacity-0 pointer-events-none'].filter(Boolean).join(" ")} ltn={false} />
    </div>
  );
};

AvailableDatesGallery.propTypes = {
  availableDates: PropTypes.array,
};

AvailableDatesGallery.defaultProps = {
  
}

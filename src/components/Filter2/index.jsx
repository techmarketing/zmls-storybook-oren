import React, { useState } from "react";
import PropTypes from "prop-types";
import { ArrowButton } from "../ArrowButton";
import { Title } from "../Title";
import { Link } from "../Link";
import { RangeInput } from "../RangeInput";
import { Divider } from "../Divider";
// import { Radio } from "../Radio";
import { MyNewCheckbox } from "../MyNewCheckbox";
import { Button } from "../Button";

export const Filter2 = ({
  languages,
  inputStep,
  maxHour,
  minHour,
  className,
  ...props
}) => {
  const [chosenLang, setChosenLang] = useState(null);
  const [chosenSex, setChosenSex] = useState(null);
  const [selectedMin, setSelectedMin] = useState(minHour);
  const [selectedMax, setSelectedMax] = useState(maxHour);

  const [resetRange, setResetRange] = useState(false);

  return (
    <div {...props}
      className={[
        className,
        "bg-lightgray min-h-screen flex flex-col justify-between",
      ]
        .filter(Boolean)
        .join(" ")}
    >
      <section>
        <div className="flex justify-between items-center p-5">
          <ArrowButton className="text-gunmetal" ltn={false} />
          <Link
            onClick={() => {
              setChosenLang(null);
              setChosenSex(null);
              setSelectedMin(minHour);
              setSelectedMin(maxHour);
              setResetRange(!resetRange);
            }}
            color="none"
            className="font-open-sans-hb-regular text-secondary text-lg"
          >
            איפוס
          </Link>
        </div>
        <div className="shadow-inner p-5">
          <div className="mb-6">
            <Title font="bold" className="text-secondary text-xl">
              סינון לפי:
            </Title>
          </div>
          <div>
            <Title font="regular" className="text-secondary text-xl">
              שעות קבלה
            </Title>
          </div>
          <div> </div>
          <div>
            <RangeInput
              resetRange={resetRange}
              min={minHour}
              max={maxHour}
              step={inputStep}
              className="mt-5 mb-8"
              labelGap="mb-6"
              labeled={true}
              onChange={(obj) => {
                setSelectedMax(obj.toName);
                setSelectedMin(obj.fromName);
              }}
            />
          </div>
          <Divider />

          <div className="my-8">
            <Title font="regular" className="text-secondary text-xl">
              שירות בשפה
            </Title>
            <div className="flex justify-between w-full gap-5 items-center mt-5">
              {languages.map((l) => (
                <Button
                  key={l}
                  onClick={() =>
                    chosenLang === l ? setChosenLang(null) : setChosenLang(l)
                  }
                  classNames={[`w-full`, chosenLang !== l && "border-gray-50"].filter(Boolean).join(" ")}
                  style={chosenLang === l ? "outline-w-color" : "outline"}
                >
                  {l}
                </Button>
              ))}
            </div>
          </div>

          <Divider />

          <div className="my-8">
            <Title font="regular" className="text-secondary text-xl">
              מגדר המטפל/ת
            </Title>
            <div className="flex gap-12 mt-5">
              <span className="flex items-center">
                <MyNewCheckbox
                    // iconComp={<div className={chosenSex==="male" && "bg-red-500"}>hello im an icon</div>}
                  checked={chosenSex === "male"}
                  onChange={() =>
                    chosenSex === "male"
                      ? setChosenSex(null)
                      : setChosenSex("male")
                  }
                  size="md"
                  className="ml-1"
                />
                <Title
                  font="regular"
                  className="text-gunmetal text-xl leading-1 align-start"
                >
                  מטפל
                </Title>
              </span>
              <span className="flex items-center">
                <MyNewCheckbox
                  checked={chosenSex === "female"}
                  onChange={() =>
                    chosenSex === "female"
                      ? setChosenSex(null)
                      : setChosenSex("female")
                  }
                  size="md"
                  className="ml-1"
                />
                <Title
                  font="regular"
                  className="text-gunmetal text-xl leading-1 align-start"
                >
                  מטפלת
                </Title>
              </span>
            </div>
          </div>

          <Divider />
        </div>
      </section>

      <section className="flex justify-center w-full pb-8">
        <Button
          disabled={
            chosenSex === null &&
            chosenLang === null &&
            selectedMin === minHour &&
            selectedMax === maxHour
          }
          classNames="w-3/4"
        >
          סינון
        </Button>
      </section>
    </div>
  );
};

Filter2.propTypes = {
  languages: PropTypes.array,
  inputStep: PropTypes.string,
  maxHour: PropTypes.string,
  minHour: PropTypes.string,
};

Filter2.defaultProps = {};

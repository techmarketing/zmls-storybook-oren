import React from 'react';


import { Filter2 } from '.';

export default {
  title: 'templates/Filter2',
  component: Filter2,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
  args: {
  languages: ['עברית', 'אנגלית', 'ערבית'],
    inputStep: "04:00",
    maxHour: "20:00",
    minHour: "08:00",
  }
};

const Template = (args) => <Filter2 {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


import React from 'react';

import { MyNewCheckbox } from '.';

export default {
  title: 'atoms/MyNewCheckbox',
  component: MyNewCheckbox,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {

  },
  args: {
    // size: "sm",
    checked: false,
    disabled: false,
    label: "לייבל",
    labelClass: "font-open-sans-hb-light",
    labelPlacement: "start"
  }
};

const Template = (args) => <MyNewCheckbox {...args} />;

const TemplateInContext = (args) => <div className={"flex items-center"}><MyNewCheckbox  {...args}/><div>Click it!</div></div>

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


export const InContext = TemplateInContext.bind({});
Primary.args = {
    // label: 'Primary',
};


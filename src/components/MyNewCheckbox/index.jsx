import React, { useMemo } from "react";
import PropTypes from "prop-types";

import CheckIcon from "@material-ui/icons/Check";

import "./cb.css";

export const MyNewCheckbox = ({ labelPlacement ,iconComp ,labelClass ,label ,className, size, checked, id, ...props }) => {
  const checkboxId = useMemo(() => {
    if (id) {
      return id;
    }
    return "mynewcb" + Math.floor(Math.random() * 987345987);
  }, [id]);

  return (
    <span className={[ "flex justify-center items-center w-max gap-2" ,labelPlacement==="top" && "flex-col-reverse", labelPlacement==="bottom" && "flex-col", labelPlacement==="end" && "flex-row-reverse"].filter(Boolean).join(" ")}>
    <span {...props} className={[className, "flex item-center gap-2",].filter(Boolean).join(" ")}>
      <input
        type="checkbox"
        id={checkboxId}
        hidden
        checked={checked || null}
        className={`mycb opacity-0 absolute pointer-events-none ${props.disabled && 'disabled'}`}
      />
      <label htmlFor={!props.disabled && checkboxId}>
        {!iconComp ? <span
          tabIndex="1"
          className={[
            " bg-white border-input-border border-2 rounded flex justify-center items-center shadow h-6 w-6",
            className,
            props.disabled ? "" : "hover:border-primary",
            // size === "sm" && "w-4 h-4",
            // size === "md" && "w-8 h-8",
            // size === "lg" && "w-12 h-12",
            // size !== "sm" && size !== "md" && size !== "lg" && size,
          ]
            .filter(Boolean)
            .join(" ")}
        >
          <span className={"select-none hidden absolute  "}>
            {<CheckIcon
              className={[
                "p-0 m-0 w-full h-full",
                // size === "sm" && "transform scale-50 w-full h-full",
                // size === "lg" && "transform scale-150 w-full h-full",
              ]
                .filter(Boolean)
                .join(" ")}
            />}
          </span>
        </span> : iconComp}
      </label>
    </span>
      {label && <span className={[labelClass, props.disabled && "select-none text-gunmetal", ].filter(Boolean).join(" ")}>{label}</span>}
    </span>
  );
};

MyNewCheckbox.propTypes = {
  size: PropTypes.oneOf(["sm", "md", "lg"]),
  className: PropTypes.string,
  label: PropTypes.string,
  labelClass: PropTypes.string,
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  labelPlacement: oneOf(['top', 'bottom', 'end', 'start'])
};

MyNewCheckbox.defaultProps = {

};

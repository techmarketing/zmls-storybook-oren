import React from "react";
import PropTypes from "prop-types";

export const Radio = ({ label, chosen, className, ...props }) => {
  return (
    <div
      {...props}
      className={[
        `
        select-none rounded-2xl border-2 font-open-sans-hb-regular w-20 h-11 text-center flex items-center justify-center
        ${
          chosen
            ? "bg-primary text-white border-primary"
            : "text-gunmetal border-gray-300"
        }`,
        props.disabled
          ? "opacity-40 cursor-not-allowed"
          : "hover:bg-primary hover:text-white hover:border-2 hover:border-white",
        className,
      ]
        .filter(Boolean)
        .join(" ")}
    >
      {label}
    </div>
  );
};

Radio.propTypes = {
  label: PropTypes.string,
};

Radio.defaultProps = {};

import React from 'react';


import { Radio } from '.';

export default {
  title: 'atoms/Radio',
  component: Radio,
  parameters: {
    backgrounds: {
      values: [
        { name: 'red', value: '#f00' },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
  },
  args: {
    label: '09:50',
    chosen: false,
    disabled: false,
  }
};

const Template = (args) => <Radio {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


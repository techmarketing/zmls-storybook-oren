import React from 'react';


import { PersonalAreaSivan } from '.';

export default {
  title: 'templates/PersonalAreaSivan',
  component: PersonalAreaSivan,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
};

const Template = (args) => <PersonalAreaSivan {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


import React from "react";
import PropTypes from "prop-types";
import { CardContainer } from "../CardContainer";
import { Title } from "../Title";
import { Divider } from "@material-ui/core";
import { Button } from "../Button";

export const PersonalAreaSivan = ({ appointmentsArray,className, ...props }) => {
  return (
    <div
      className={[className, "bg-lightgray min-h-screen"]
        .filter(Boolean)
        .join(" ")}
    >
      <CardContainer>
        <div className="flex">
          <div className="w-1/12">ast</div>
          <Title font="regular" className="text-gunmetal text-lg">
            התורים שלי
          </Title>
        </div>
        <div className="my-5">
          <div className="">SOME APPOINTMENTS</div>
          {appointmentsArray &&
            appointmentsArray.map((a) => <div>hello from {a.name}</div>)}
        </div>
        <Divider className="my-4" />
        <div className="flex justify-center items-center">
          <Button
            style="naked"
            classNames=""
          >
            התנתקות
          </Button>
        </div>
      </CardContainer>
      <div>
        <Button
          style="outline-w-color"
          classNames=""
        >
          התורים שלי
        </Button>
      </div>
      <CardContainer className="p-5 pb-6 mt-3 shadow-sm">
        <div className="mb-12">
          <Title
            font="light"
            className="text-subtitle-gray tracking-wide text-xl"
          >
            אצלנו בכללית רפואה משלימה כל טיפול מתחיל בפגישת ייעוץ עם רופא שבונה
            את תוכנית הטיפול הטובה ביותר עבורך{" "}
          </Title>
        </div>
        <div className="flex justify-center px-3">
          <Button classNames="">
            זמני תור לייעוץ +
          </Button>
        </div>
      </CardContainer>
    </div>
  );
};

PersonalAreaSivan.propTypes = {
  className: PropTypes.string,
};

PersonalAreaSivan.defaultProps = {
  className: "p-2",
};

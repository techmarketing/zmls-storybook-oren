import React from 'react'
import PropTypes from 'prop-types'
import { ArrowButton } from '../ArrowButton'
import { ProfileAvatar } from '../ProfileAvatar'
import { Title } from '../Title'
import { Link } from '../Link'


export const Header = ({ backBtn ,type ,className, logoWidth ,loggedIn ,...props}) => {
    return (
        type === "login" ? 
        <div className={[
            "flex justify-center items-center relative bg-white p-1 h-12",
            className || "",
            // loggedIn ? 'justify-between' : 'justify-center',
        ].filter(Boolean).join(" ")}>
            {loggedIn ? <ProfileAvatar className="absolute left-5">סג</ProfileAvatar> : ''} 
            <img className={[logoWidth || "w-1/4 md:w-1/6","max-w-16"].filter(Boolean).join(" ")} src="https://www.clalitmashlima.co.il/tm-content/uploads/2017/09/cropped-logo.png" alt="clalit mushlam" />
            {backBtn ? <ArrowButton className="text-gunmetal absolute right-5" ltn={false}/> : ''} 
        </div> :
        <div className={[
            "flex justify-center items-center relative h-12 p-1 bg-subtitle-blue",
            className
            // loggedIn ? 'justify-between' : 'justify-center',
        ].filter(Boolean).join(" ")}>
            <Link className="absolute left-5 text-white font-open-sans-hb-regular" color="white" >איפוס</Link>
            <Title className="text-white font-open-sans-hb-regular text-xl"  titleType="different">סינון לפי:</Title>
            <ArrowButton className="text-white absolute right-5" ltn={false}/> 
        </div>
    )
}

Header.propTypes = {
    loggedIn: PropTypes.bool,
    type: PropTypes.string
}

Header.defaultProps = {
}
import React from 'react';


import { Header } from '.';

export default {
  title: 'molecules/Header',
  component: Header,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
  args: {
    loggedIn: true,

  }
};

const Template = (args) => <Header {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


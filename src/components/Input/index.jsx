import React, { useState } from "react";
import PropTypes from "prop-types";
import SearchIcon from '@material-ui/icons/Search';


export const Input = ({changeFoo ,wrapperWidth ,className, inputType, ...props }) => {
  // the input validator will validate and format the input value based on the type prop

  const [inputVal, setInputVal] = useState("")

  const theInputValidator = (e) => {
    let val = e.target.value.split("-").join("");
    let val2 = e.target.value.split("/").join("");

    const digitsOnlyReg = new RegExp("^[0-9]+$");
    const expDateReg = /^(0[1-9]|1[0-2])\/?([0-9]{4}|[0-9]{2})$/;
    switch (inputType) {
      case "credit-card":
        // up to 16 digits, with '-' every 4, no letters
        if (val.length > 16) {
          e.target.value = val.substring(0, val.length - 1);
          val = e.target.value.split("-").join("");
          const parts = val.match(/.{1,4}/g);
          e.target.value = parts.join("-");
          return;
        }
        console.log(val.length);
        if (digitsOnlyReg.test(val)) {
          const parts = val.match(/.{1,4}/g);
          e.target.value = parts.join("-");
          setInputVal(val)
          return;
        }
        e.target.value = val.substring(0, val.length - 1);

      case "exp-date":
        //MM/YY

        if (!digitsOnlyReg.test(val2) || val2.length > 4) {
          e.target.value = val2.substring(0, val2.length - 1);
          val2 = e.target.value.split("/").join("");
          const parts = val2.match(/.{1,2}/g);
          e.target.value = parts.join("/");
          return;
        }

        const parts = val.match(/.{1,2}/g);
        e.target.value = parts.join("/");
        setInputVal(val)
        return;

      case "cvv":
        // 3 digits only
        if (!digitsOnlyReg.test(val) || val.length > 3) {
          e.target.value = val.substring(0, val.length - 1);
        }
        setInputVal(val)
        return;

      case "full-name":
        // no digits
        const includingDigitsReg = new RegExp(".*[0-9].*");
        if (includingDigitsReg.test(val) || val.split(" ").length > 8) {
          e.target.value = val.substring(0, val.length - 1);
        }
        setInputVal(val)
        return;

      case "id":
        // 9 digits

        if (!digitsOnlyReg.test(val) || val.length>9) {
          e.target.value = val.substring(0, val.length - 1);
          return;
        }
        setInputVal(val)
        return;

      case "phone":
        // 10 digits

        if (!digitsOnlyReg.test(val) || val.length>10) {
          e.target.value = val.substring(0, val.length - 1);
          return;
        }
        setInputVal(val)
        return;

      case "search":
        setInputVal(e.target.value)

      default:
        break;
    }
  };

  return (
    <span className={["relative flex items-center", wrapperWidth].filter(Boolean).join(" ")}>
      <input
        {...props}
        type="text"
        className={[
          "outline-none w-full rounded border-2 border-input-border h-10 py-1.5 px-5 text-xl text-gunmetal",
        className,
          inputType === "credit-card" && "",
        ]
          .filter(Boolean)
          .join(" ")}
        onChange={(e) => {
          theInputValidator(e);
          changeFoo(e)
        }}
      />

      {inputType === "credit-card" && (
        <img
          className="absolute w-8 top-2 left-2"
          src="https://banner2.cleanpng.com/20180315/jlq/kisspng-credit-card-debit-card-computer-icons-credit-card-cliparts-5aaac2206bc645.2454962615211402564415.jpg"
          alt=""
        />
      )}

      {inputType === "search" && inputVal.length===0 && (
        <SearchIcon
        className="absolute w-8 left-2 text-zmls-gray-300"
        />

      )}

    </span>
  );
};

Input.propTypes = {
  dir: PropTypes.oneOf(["ltr", "rtl"]),
  inputType: PropTypes.oneOf([
    "freedom",
    "credit-card",
    "exp-date",
    "cvv",
    "full-name",
    "id",
    "search",
    "phone",
  ]),
  className: PropTypes.string,
};

Input.defaultProps = {
  // inputType:"exp-date"
};

import React from 'react';


import { Input } from '.';

export default {
  title: 'atoms/Input',
  component: Input,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
  args: {
    dir: "rtl",
    inputType: "phone",
    className: "w-1/2",
  }
};

const Template = (args) => <Input {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


import React from "react";

import { ProfileAvatar } from ".";

export default {
  title: "atoms/ProfileAvatar",
  component: ProfileAvatar,
  parameters: {
    backgrounds: {
      values: [
        { name: "red", value: "#f00" },
        { name: "green", value: "#0f0" },
        { name: "blue", value: "#00f" },
      ],
    },
  },
  argTypes: {
    className: {
      name: "CSS Classes",
      control: { type: "text" },
    },
  },
  args: {
    children: "OS",
  },
};

const Template = (args) => <ProfileAvatar {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  // date: Da
};

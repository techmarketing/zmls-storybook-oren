import React from "react";
import PropTypes from "prop-types";

export const ProfileAvatar = ({ className, ...props }) => {
  return (
    <div
      {...props}
      className={[
        "w-8 h-8 bg-azure rounded-full text-white flex justify-center items-center text-sm",
        className,
      ]
        .filter(Boolean)
        .join(" ")}
    ></div>
  );
};

ProfileAvatar.propTypes = {
  children: PropTypes.string,
};

ProfileAvatar.defaultProps = {};

import React from "react";
import PropTypes from "prop-types";
import { CardContainer } from "../CardContainer";
import { Title } from "../Title";
import { Button } from "../Button";

export const NextAppointmentCard = ({ appointment, className, ...props }) => {
  return (
    <div {...props} className={[className, ""].filter(Boolean).join(" ")}>
      <CardContainer className="flex flex-col w-[70vw] gap-2 p-4 box-border">
        <Title
          font="bold"
          className="text-gunmetal tracking-wider"
        >{"טיפול " + appointment.name}</Title>
        {Object.keys(appointment).map((topic) =>
          topic !== "tipulName" ? (
            <div className="flex">
              <div className="w-2/12">ast</div>
              <Title
                className=" text-md tracking-wider text-gunmetal"
                font="regular"
              >{appointment[topic]}</Title>
            </div>
          ) : (
            ""
          )
        )}
        <div className="flex gap-2 box-border">
          <Button
            classNames="w-1/2"
          >עדכון תור</Button>
          <Button
            classNames="w-1/2"
            style="outline"
          >ביטול תור</Button>
        </div>
      </CardContainer>
    </div>
  );
};

NextAppointmentCard.propTypes = {
  appointment: PropTypes.object,
};

NextAppointmentCard.defaultProps = {
  
};

import React from 'react';


import { NextAppointmentCard } from '.';

export default {
  title: 'molecules/NextAppointmentCard',
  component: NextAppointmentCard,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
  args: {
    appointment: {
      name: "טווינא",
      clinic: "מרפאת אשקלון סנטר",
      where: "גבעת השקמה 2, קריית אונו",
      date: "05.02.20 | יום ד׳",
      hour: "09:40",
      dr: "מטפל: ירושלמי בן הרוש שלומי",
    },
  }
};

const Template = (args) => <NextAppointmentCard {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


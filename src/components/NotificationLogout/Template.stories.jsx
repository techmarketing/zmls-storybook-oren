import React from 'react';


import { NotificationLogout } from '.';

export default {
  title: 'molecules/NotificationLogout',
  component: NotificationLogout,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
};

const Template = (args) => <div className="h-screen p-4 bg-lightgray"><NotificationLogout {...args} /></div>;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


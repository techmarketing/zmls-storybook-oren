import React from "react";
import { CardContainer } from "../CardContainer";
import { Title } from "../Title";
import { Button } from "../Button";
import CloseIcon from "@material-ui/icons/Close";

export const NotificationLogout = ({ className, ...props }) => {
  return (
    <CardContainer
      {...props}
      className={[className, "relative pb-6"].filter(Boolean).join(" ")}
    >
      <CloseIcon className="absolute right-5 top-5 text-gunmetal" />
      <div className="flex justify-center items-center h-24 text-3xl">ast</div>
      <div className="flex justify-center items-center">
        <Title className="text-center text-secondary text-3xl" font="bold">האם ברצונך להתנתק?</Title>
      </div>
      <div className="flex justify-center items-center">
        <Button style="outline-w-color" classNames="w-[60%] my-12">התנתקות</Button>

      </div>
      {/* <div className="flex justify-center items-center">
      </div> */}
    </CardContainer>
  );
};

NotificationLogout.propTypes = {};

NotificationLogout.defaultProps = {};

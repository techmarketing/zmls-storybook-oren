import React from "react";

import { AvatarGallery } from ".";

export default {
  title: "molecules/AvatarGallery",
  component: AvatarGallery,
  parameters: {
    backgrounds: {
      values: [
        { name: "fun gray", value: "#f7f9fc", default: true },
        { name: "green", value: "#0f0" },
        { name: "blue", value: "#00f" },
      ],
    },
  },
  argTypes: {},
  args: {
    profilesArr: [
      { fname: "דולב", lname: "גרוזנברג", id: "dolev_id" },
      { fname: "יוני", lname: "יוניהו", id: "yoni_id" },
      { fname: "קרן", lname: "ישראלשווילי", id: "israel_id" },
      { fname: "מאיה", lname: "ישראלשווילי", id: "sami_id" },
      { fname: "עופר", lname: "ישראלשווילי", id: "ofer_id" },
    ],
    className: "w-1/3 flex justify-end",
    height: 16,
  },
};

const Template = (args) => (
  <div className="bg-lightgray min-h-screen flex justify-center items-center">
    <AvatarGallery {...args} />
  </div>
);

export const Primary = Template.bind({});
Primary.args = {
  // label: 'Primary',
};

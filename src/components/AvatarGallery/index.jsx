import React, { useState } from 'react'
import PropTypes from 'prop-types'
import PersonalSpaceAvatar, { supportedColors } from '../PeronsalSpaceAvatar'
import { ArrowButton } from '../ArrowButton'

export const AvatarGallery = ({   fatherFoo2 ,fatherFoo1 ,profilesArr , height, className, ...props}) => {

    const [currIndex, setCurrIndex] = useState(0)

    const handleArrowClick = () => {
        if(currIndex===profilesArr.length-1){
            // console.log(currIndex)
            fatherFoo2 && fatherFoo2()
            fatherFoo1 && fatherFoo1(0)
            return setCurrIndex(0)
        }
        // console.log(currIndex)
        fatherFoo2 && fatherFoo2()
        fatherFoo1 && fatherFoo1(currIndex+1)
        return setCurrIndex(currIndex+1)
        
    }

    return (
        <div {...props}
        className={[
            className || "w-1/2",
            "flex flex-row-reverse justify-between items-center",
            "h-"+height,
        ].filter(Boolean).join(" ")}
        >

                <div className="flex relative flex-grow justify-start items-center">
                    {profilesArr?.map((p, i)=><div key={i} className="absolute flex items-center flex-row-reverse">
                    
                    <ArrowButton className="text-gunmetal transform -translate-x-6" onClick={()=>handleArrowClick()}/>
                    <PersonalSpaceAvatar
                    current={i===currIndex}
                    colorIndex={Math.floor(Math.random()*supportedColors.length)}
                    className={[
                        "tranition-opacity duration-200 ease-in-out shadow",
                        i===currIndex ? "z-20" : "pointer-events-none",
                        "w-"+height, "h-"+height,
                        i===currIndex-1 && "transform translate-x-1/2 scale-75 z-10 opacity-0",
                        currIndex===0 && i===profilesArr.length-1 && "transform translate-x-1/2 scale-75 z-10 opacity-0",
                        i===currIndex+1 && "transform -translate-x-1/2 scale-75 z-10 opacity-50",
                        i===0 && currIndex===profilesArr.length-1 && "transform -translate-x-1/2 scale-75 z-10 opacity-50",
                        i!==currIndex && i!==currIndex+1 && currIndex!==profilesArr.length-1 && "opacity-0",
                        i!==currIndex && i!==currIndex+1 && i!==currIndex+2 && currIndex!==profilesArr.length-1 && "opacity-0 pointer-events-none",
                    ].filter(Boolean).join(" ") }
                    >{p.fname}</PersonalSpaceAvatar></div>)}
                </div>

        </div>
    )
}

AvatarGallery.propTypes = {
    profilesArr: PropTypes.array,
    className: PropTypes.string,
    height: PropTypes.number
}

AvatarGallery.defaultProps = {
    
} 
import React from "react";

import { CardContainer } from ".";

export default {
  title: "atoms/CardContainer",
  component: CardContainer,
  parameters: {
    backgrounds: {
      values: [
        { name: "fun gray", value: "#f7f9fc", default: true },
        { name: "green", value: "#0f0" },
        { name: "blue", value: "#00f" },
      ],
    },
  },
  argTypes: {},
};

const Template = (args) => (
  <CardContainer {...args}>
    <ul>
      <li>David</li>
      <li>Haim</li>
    </ul>
    <div>asd</div>
  </CardContainer>
);

export const Primary = Template.bind({});
Primary.args = {
  
};

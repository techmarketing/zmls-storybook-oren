import React from 'react'
import {createDivComponent} from '../../utils/createDivComponent';

// const CardContainer = createDivComponent('rounded bg-white shadow-md p-4');
export const CardContainer = createDivComponent('rounded bg-white border-2 p-4');

// export default CardContainer;
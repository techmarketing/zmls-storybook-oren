import React from 'react'
import { PersonalSpace_old_00 } from '../PersonalSpace_old_00'
import { Header } from '../Header'
import { TipulimMeusharimMolecule } from '../TipulimMeusharimMolecule'
import { NextAppointmentsMolecule } from '../NextAppointmentsMolecule'
import { LastAppointmentsMolecule } from '../LastAppointmentsMolecule'


export const PersonalAreaFull_old_00 = ({className, ...props}) => {
    return (
        <div
        className={[
            className,
            "h-screen overflow-x-auto bg-lightgray"
        ].filter(Boolean).join(" ")}
        >
            <Header className="py-5 bg-white" type="login"/>
            <PersonalSpace_old_00 className="bg-white pt-5 shadow-inner" />
            <div className="p-5">
                <NextAppointmentsMolecule />
                <LastAppointmentsMolecule />
                <TipulimMeusharimMolecule className="" />
            </div> 
        </div>
    )
}

PersonalAreaFull_old_00.propTypes = {
    
}

PersonalAreaFull_old_00.defaultProps = {
}
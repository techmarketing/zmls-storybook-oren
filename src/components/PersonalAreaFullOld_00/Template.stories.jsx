import React from 'react';


import { PersonalAreaFull_old_00 } from '.';

export default {
  title: 'old/templates/PersonalAreaFull_old_00',
  component: PersonalAreaFull_old_00,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
};

const Template = (args) => <PersonalAreaFull_old_00 {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


import React from 'react';


import { ClinicCard } from '.';

export default {
  title: 'molecules/ClinicCard',
  component: ClinicCard,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
  args: {
    clinicObj: {
      name: "קיראון - כללית רפואה משלימה - קריית אונו",
      address: "השיקמה 2, קריית אונו",
      distance: 9.1,
      nextAppointment: "28.02.2020",
  }
  }
};

const Template = (args) => <ClinicCard {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


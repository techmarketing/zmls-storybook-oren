import React from 'react'
import PropTypes from 'prop-types'
import { Title } from '../Title'
import { Button } from '../Button'

export const ClinicCard = ({ clinicObj ,className, ...props}) => {
    return (
        <div {...props} 
        className={[
            className,
            "flex flex-col gap-2"
        ].filter(Boolean).join(" ")}
        >
            <div className="flex justify-start items-start">
                {/* <div className="px-3">ast</div> */}
                <Title  font="bold" className="text-xl text-secondary pr-3 " >{clinicObj?.name}</Title>
            </div>
            <div className="flex justify-start gap-1 items-center">
                <div className="px-3">ast</div>
                <Title  font="regular" className="text-gunmetal text-xl tracking-wide" >{`כתובת: ${clinicObj?.address}`}</Title>
            </div>
            <div className="flex justify-start gap-1 items-center">
                <div className="px-3">ast</div>
                <Title font="regular" className="text-gunmetal text-xl tracking-wide">{`מרחק מהבית שלך: ${clinicObj?.distance} ק״מ`}</Title>
            </div>
            <div className="flex justify-start gap-1 items-center">
                <div className="px-3">ast</div>
                <Title  font="regular" className="text-gunmetal text-xl tracking-wide" >{`מועד התור הפנוי ביותר: ${clinicObj?.nextAppointment}`}</Title>
            </div>
            <div className="flex justify-center items-center my-3">
                <Button classNames="w-[60%]"  >לכל התורים</Button>
            </div>

            
        </div>
    )
}

ClinicCard.propTypes = {
    clinicObj: PropTypes.object
}

ClinicCard.defaultProps = {
    
}
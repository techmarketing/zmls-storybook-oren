import React from 'react'
import PropTypes from 'prop-types'

export const Title = ({ color ,className, font ,...props}) => {
    return (
        <span
        {...props}
        className={[
            'select-none',
            font==="regular" && "font-open-sans-hb-regular",
            font==="bold" && "font-open-sans-hb-bold",
            font==="light" && "font-open-sans-hb-light",
            color==="primary" && "text-primary",
            color==="secondary" && "text-secondary",
            color==="error" && "text-error",
            color==="gunmetal" && "text-gunmetal",
            className,
        ].filter(Boolean).join(" ")}
        >
        
        </span>
    )
}

Title.propTypes = { 
    font: PropTypes.oneOf(['regular', 'bold','light']),
    color: PropTypes.oneOf(['primary', 'secondary','error', 'gunmetal']),
}

Title.defaultProps = { 
    // dir: "rtl",
    color: "primary"
}

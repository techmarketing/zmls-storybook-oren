import React from 'react';

import { Title } from '.';

export default {
  title: 'atoms/Title',
  component: Title,
  parameters: {
    backgrounds: {
      values: [
        { name: 'red', value: '#f00', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {

  },
  args: {
    children: "זימון תור ללא סיסמא",
    font:  "regular",
    className:  "",
  }
};

const Template = (args) => <Title {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


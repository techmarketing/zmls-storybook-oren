import React from 'react';


import { ZMLSRefuaMashlima } from '.';

export default {
  title: 'templates/ZMLSRefuaMashlima',
  component: ZMLSRefuaMashlima,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
};

const Template = (args) => <ZMLSRefuaMashlima {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


import React, { useState } from 'react'
import { Header } from '../Header'
import { Title } from '../Title'
import { Input } from '../Input'
import { Button } from '../Button'

export const ZMLSRefuaMashlima = ({className, ...props}) => {

    const [inputVal, setInputVal] = useState("")

    return (
        <div {...props}
        className={[
            className,
            "bg-lightgray h-screen overflow-auto pb-3"

        ].filter(Boolean).join(" ")}
        >
            <Header loggedIn={false} className="bg-white py-4" logoWidth="w-1/3" type={"login"} />
            <div className="shadow-inner px-4">
                <div className="flex justify-center items-center pt-3">
                    <Title size="md" titleType="header" className="font-open-sans-hb-bold leading-8 text-center" >זימון תור רפואה משלימה</Title>
                </div>

                <div className="flex items-center mb-8">
                <Title 
                className="text-center leading-5 text-gunmetal w-full text-md tracking-wider mt-1 font-open-sans-hb-regular"
                font="regular"
                >זימון תור ללא סיסמא ללקוחות כללית אונליין</Title>

                </div>

                <Title 
                className="mt-8 text-gunmetal tracking-wide text-md"
                font="regular"
                >מספר תעודת זהות</Title>

                <Input changeFoo={(e)=>setInputVal(e.target.value)} className="w-full mt-3 p-1" inputType="id" />

                <div className="flex justify-center items-center my-8">CAPTCHA</div>

                <div className="flex justify-center items-center my-6">
                    <Button disabled={!inputVal} type="submit" classNames="">שלחו לי קוד לנייד</Button>
                </div>


            </div>
        </div>
    )
}

ZMLSRefuaMashlima.propTypes = {
    
}

ZMLSRefuaMashlima.defaultProps = {

}
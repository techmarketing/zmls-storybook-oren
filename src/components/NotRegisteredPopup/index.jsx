import React from "react";
import PropTypes from "prop-types";
import { CardContainer } from "../CardContainer";
import { Title } from "../Title";
import CloseIcon from "@material-ui/icons/Close";
import { Button } from "../Button";

export const NotRegisteredPopup = ({ className, ...props }) => {
  return (
    <CardContainer {...props}
      className={[
        "flex justify-center items-center flex-col relative",
        className,
      ]
        .filter(Boolean)
        .join(" ")}
    >
      <CloseIcon className="absolute right-5 top-5" />
      <div className="my-8">ast</div>
      <Title
        font="bold"
        className="text-secondary text-2xl text-center px-1"
      >נראה שלא נרשמת לשירות</Title>
      <div className="p-2 flex justify-center">
        <Title
          font="regular"
          className="text-secondary text-xl text-center mt-8"
        >יש להיכנס לתיק הרפואי האישי באתר כללית און ליין ולהירשם באופן חד פעמי לשירות</Title>
      </div>
      <Button
        style="outline-w-color"
        classNames="w-4/6"
      >להרשמה</Button>
    </CardContainer>
  );
};

NotRegisteredPopup.propTypes = {};

NotRegisteredPopup.defaultProps = {};

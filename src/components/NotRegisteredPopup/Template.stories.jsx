import React from 'react';


import { NotRegisteredPopup } from '.';

export default {
  title: 'molecules/NotRegisteredPopup',
  component: NotRegisteredPopup,
  parameters: {
    backgrounds: {
      values: [
        { name: 'fun gray', value: '#f7f9fc', default:true },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  argTypes: {
    
  },
};

const Template = (args) => <div className="h-screen bg-lightgray p-4"><NotRegisteredPopup className="px-1 py-7" {...args} /></div>;

export const Primary = Template.bind({});
Primary.args = {
    // label: 'Primary',
};


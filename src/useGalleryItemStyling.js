import { useCallback, useMemo } from "react";


const useGalleryItemStyling = (maxItems, gaps = [.7], scaling = .7) => {
    const calculateStyles = useCallback((pos) => {
      // console.log('calc', i++);
      const max = Math.ceil((maxItems) / 2);
      const visibleSide = Math.floor((maxItems) / 2);
  
      const _pos = Math.max(-max, Math.min(max, pos))
  
      if(Math.abs(_pos) < Math.abs(pos)) {
        return {display: 'none'}
      }
  
      const translateX = new Array(Math.abs(_pos)).fill(0).reduce((sum, item, i) => {
        return sum + (100 * gaps[Math.min(i, gaps.length -1)] * Math.pow(scaling, i));
      }, 0) * (_pos < 0 ? -1 : 1);
  
      return {
        zIndex: max - Math.abs(_pos),
        transform: `translateX(${translateX}%) scale(${Math.pow(scaling, Math.abs(_pos))})`,
        opacity: Math.abs(_pos) <= visibleSide ? 1 : 0
      }
    }, [maxItems, gaps, scaling]);
  
    const cache = useMemo(() => {
      return new Array(maxItems * 2 + 1).fill(0).reduce((cache, item, index) => {
        return {
          ...cache,
          [index - maxItems]: calculateStyles(index - maxItems)
        }
      }, {})
    }, [maxItems/*, gaps, scaling*/])
  
    const getStyles = pos => cache.hasOwnProperty(pos) ? cache[pos] : {display: 'none'};
  
    return getStyles;
  }

  export default useGalleryItemStyling;
const stringToNum = (string) => {
    if (typeof string !== "string" || string.indexOf(":") === -1) {
      return string;
    }
    const [hour = 0, min = 0] = string.split(":");
    return parseInt(hour) * 60 + parseInt(min);
  };
  
  const numToString = (num) => {
    const zerofy = (n) => ("0" + n).slice(-2);
    return `${zerofy(Math.floor(num / 60))}:${zerofy(num % 60)}`;
  };

  module.exports = {
      stringToNum,
      numToString
  }
const datePickerArranger = (year = new Date().getFullYear(), month = new Date().getMonth()+1, weekLength = 6, hebrew = true) => {
    // const date = new Date();
    month--;
    const firstOfTheMonth = new Date(year, month, 1)
    const weekdayOfFirst = firstOfTheMonth.getDay()

    console.log(new Date(year, month))

    
    const remainderBefore = 7 - (7 - weekdayOfFirst)

    const lastDayOfPrevMonth = new Date(year, month, 0).getDate()

    const lastDayOfThisMonth = new Date(year, month+1, 0).getDate()
    
    const week1 = []
    const week2 = []
    const week3 = []
    const week4 = []
    const week5 = []

    // if remainder is equal to week's length, do a different for loop.

    for (let i = 0; i < remainderBefore; i++) {
        week1.unshift(lastDayOfPrevMonth - i)
    }


    console.log(remainderBefore)
    
    
    
    for (let i = 1; i <= 7-remainderBefore; i++) {
        week1.push(i)
    }

    console.log(week1)
    
    for (let i = week1[6]+1; i <= lastDayOfThisMonth; i++) {
        if(week2.length<7){
            week2.push(i)
            continue
        }
        if(week3.length<7){
            week3.push(i)
            continue
        }
        if(week4.length<7){
            week4.push(i)
            continue
        }
        if(week5.length<7){
            week5.push(i)
            continue
        }
    }

    if(week5.length<7){
        for (let i = 1; i <= 8-week5.length; i++) {
            week5.push(i)
        }
    }

    const arrangedWeeks = [ 
        week1, week2, week3, week4 , week5
    ]

    if(weekLength){
        for (const week of arrangedWeeks) {
            week.length = weekLength
        }
    }
    if(hebrew){
        for (const week of arrangedWeeks) {
            week.reverse()
        }
    }
    console.log(arrangedWeeks)
    return arrangedWeeks

}

export default datePickerArranger

export const useDatePickerArragner = (year, month, weekLength) => {
    return useMemo(() => datePickerArranger(year, month + 1, weekLength), [year, month, weekLength]);
}
const useDatePickerArragner = (date = new Date(), monthsToShow = 1, weekdaysToRemove = []) => {

    const originalMonth = date.getMonth()
    const originalYear = date.getFullYear()

    // the function returns an object of arrays of arrays that are printed for each week of a month

    // date - the grid is based on it

    // this obj will contain arrays representing months that will contains arrays representing weeks
    const calendarData = {}

    // for loop that iterates for each month to show, on first iteration determines

    for (let i = 0; i < monthsToShow; i++) {

        currDate = new Date(originalYear, (originalMonth + i), 1)
        
        // console.log("currDate: ", currDate)

        const firstOfTheMonth = new Date(currDate.getFullYear(), currDate.getMonth(), 1)

        const weekdayOfFirst = firstOfTheMonth.getDay()
        // console.log("weekdayOfFirst: ", weekdayOfFirst)
    
        const lastDayOfPrevMonth = new Date(currDate.getFullYear(), currDate.getMonth(), 0).getDate()
        // console.log("last day of prev month: ", lastDayOfPrevMonth)
        
        const lastDayOfThisMonth = new Date(currDate.getFullYear(), currDate.getMonth() + 1, 0).getDate()
        // console.log("last day of this month: ", lastDayOfThisMonth)
        
        // determine how many weeks we have in this month
        week1 = []
        week2 = []
        week3 = []
        week4 = []
        week5 = []
        week6 = []
        
        for (let j = 0; j < weekdayOfFirst; j++) {
            // week1.unshift(new Date(currDate.getFullYear() ,lastDayOfPrevMonth - j))
            week1.unshift(new Date(originalYear, originalMonth + i - 1, lastDayOfPrevMonth - j))
        }

        
        for (let j = 1; j <= 7 - weekdayOfFirst; j++) {
            week1.push(new Date(originalYear, originalMonth + i ,j))
        }
        // console.log(week1)

        // console.log(week1)

        for (let j = week1[6].getDate()+1; j <= lastDayOfThisMonth; j++) {
            if(week2.length<7){
                // week2.push(j)
                week2.push(new Date(originalYear, originalMonth + i ,j))
                continue
            }
            if(week3.length<7){
                // week3.push(j)
                week3.push(new Date(originalYear, originalMonth + i ,j))
                continue
            }
            if(week4.length<7){
                // week4.push(j)
                week4.push(new Date(originalYear, originalMonth + i ,j))
                continue
            }
            if(week5.length<7){
                // week5.push(j)
                week5.push(new Date(originalYear, originalMonth + i ,j))
                continue
            }
            if(week6.length<7){
                // week6.push(j)
                week6.push(new Date(originalYear, originalMonth + i ,j))
                continue
            }
        }

        // fill up the last week
        if(week4.some(d=>d.getDate()===lastDayOfThisMonth) && week4.length!==7){
            for (let j = 1; week4.length !== 7; j++) {
                week4.push(new Date(originalYear, originalMonth + i + 1 ,j))
            }
        }
        else if(week5.some(d=>d.getDate()===lastDayOfThisMonth) && week5.length!==7){
            for (let j = 1; week5.length !== 7; j++) {
                week5.push(new Date(originalYear, originalMonth + i + 1 ,j))
            }
        }
        else if(week6.some(d=>d.getDate()===lastDayOfThisMonth) && week6.length!==7){
            for (let j = 1; week6.length !== 7; j++) {
                week6.push(new Date(originalYear, originalMonth + i + 1 ,j))
            }
        }

        // remove unwanted weekdays
        if(weekdaysToRemove.length>0){
            week1 = week1.filter((day, i)=>!weekdaysToRemove.some((index)=>i===index))
            week2 = week2.filter((day, i)=>!weekdaysToRemove.some((index)=>i===index))
            week3 = week3.filter((day, i)=>!weekdaysToRemove.some((index)=>i===index))
            week4 = week4.filter((day, i)=>!weekdaysToRemove.some((index)=>i===index))
            week5 = week5.filter((day, i)=>!weekdaysToRemove.some((index)=>i===index))
            week6 = week6.filter((day, i)=>!weekdaysToRemove.some((index)=>i===index))
        }

        calendarData[currDate.toLocaleString('default', { month: 'long' }) + currDate.getFullYear()] = [week1, week2, week3, week4, week5, week6]
        
    }

    // console.log(calendarData)

    return calendarData

}

module.exports = { useDatePickerArragner }
import logo from './logo.svg';
import './App.css';
import {Button} from './components/Button';
import {Radio} from './components/Radio/Radio';
import { Tab } from './components/Tabs';
import { Input } from './components/Input';
import { ArrowButton } from './components/ArrowButton';
import { AvatarGallery2 } from './components/AvatarGallery2';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p className="text-red-500">
          This is react with tailwindcss and storybook!
        </p>


        <Button 
        content="some more content"
        bgColor="bg-red-500"
        paddingX="px-8"
        paddingY="py-8"
        borderColor="border-black"
        borderWidth="border-8"
        />

        <Radio />

        <Tab />

        <Input />

        <ArrowButton />

        <AvatarGallery2 />

        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;

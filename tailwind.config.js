module.exports = {
  mode: 'jit',
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: { 
      fontFamily: {
        mono: ['Arial'],
        'header': ['OronMFW-Bold', 'Arial'],
        'subtitle': ['OronMFW-Medium', 'Arial'],
        'open-sans-hb-regular': ['open-sans-hb-regular', 'Arial'],
        'open-sans-hb-light': ['open-sans-hb-light', 'Arial'],
        'open-sans-hb-bold': ['open-sans-hb-bold', 'Arial'],
        'open-sans-hb-extrabold': ['open-sans-hb-extrabold', 'Arial'],
      },
     colors:{
       'primary': '#25ab88',
       'error': '#808080',
       'secondary': '#013457',
       'input-border':'#c3c7cb',
       'personal-space-avatar-green':'#cdeab1',
       'personal-space-avatar-blue':'#aad8ee',
       'personal-space-avatar-orange':'#f1b247',
       'azure':'#00aeef',
       'gunmetal':'#4a4a55',
       'subtitle-blue':'#013457',
       'subtitle-golden':'#f1b247',
       'subtitle-gray':'#848383',
       "lightgray":"#f7f9fc",
       "zmls-gray-300": "#5d5858",
       "pencil-gray-normal": "#9e9e9e",
       "pencil-gray-light": "#dfdfdf",
       "very-light-blue":"#e6e9ec",
       "duck-egg-blue":"#cbebe3",
       "coral":"#ff4e4e",
  
       
     },
    },
  },
  variants: {
    extend: {
      backgroundColor:['disabled'],
      textColor:['disabled'],
    },
  },
  plugins: [
    
  ],
}
